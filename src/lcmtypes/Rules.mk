LCMTYPES  	:= $(shell ls $(D)/../../lcmtypes/*.lcm)
SRC_$(D)	:= $(LCMTYPES:%.lcm=%.c)
SRC_$(D)	:= $(notdir $(SRC_$(D)))
SRC_$(D) 	:= $(addprefix $(D)/,$(SRC_$(D)))
H_$(D)		:= $(SRC_$(D):%.c=%.h)

bd_$(D) = $(realpath $(D)/../../)


$(bd_$(D))/src/lcmtypes/%.c: $(bd_$(D))/lcmtypes/%.lcm
	@echo "    $<"
	lcm-gen -c --c-typeinfo --c-cpath $(dir $@) --c-hpath $(dir $@) --cinclude lcmtypes/ $<

#threading weirdness....
$(bd_$(D))/src/lcmtypes/%.h: $(bd_$(D))/lcmtypes/%.lcm
	@echo "    $<"
	lcm-gen -c --c-typeinfo --c-cpath $(dir $@) --c-hpath $(dir $@) --cinclude lcmtypes/ $<

OBJS_$(D)	:= $(SRC_$(D):%.c=%.o)

$(OBJS_$(D)): $(SRC_$(D)) $(H_$(D))

####################################################

$(OBJS_$(D)): $(LIB_LCM)
LIBLCMTYPES_$(D) = $(LIB_PATH)/libsclcmtypes.a
LIBLCMTYPES_SO_$(D) = $(LIB_PATH)/libsclcmtypes.so

###################################################

$(LIBLCMTYPES_$(D)): $(OBJS_$(D))
		@echo "$@"
		$(AR)

TGTS		:= $(TGTS) $(SRC_$(D)) $(LIBLCMTYPES_$(D)) $(LIBLCMTYPES_SO)
CLEAN		:= $(CLEAN) $(OBJS_$(D))
