/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include "image_utils.h"

void velodyne32_depth_image_write_pgm(const image_t *image, const char *path) {
  FILE *f = fopen(path, "wb");
  
  int tmp;  
  unsigned char *p = (unsigned char*)image->data;
  for(int i = 0; i < image->width * image->height; i++) {
    tmp = *p; 
    *p = *(p + 1); 
    *(p + 1) = tmp; 
    p += 2;
  }
  fprintf(f, "P5\n%d %d\n65535\n", image->width, image->height);
  fwrite(image->data, sizeof(unsigned short int), image->width * image->height, f);
}

void velodyne32_intensity_image_write_pgm(const image_t *image, const char *path) {
  FILE *f = fopen(path, "wb");
  
  fprintf(f, "P5\n%d %d\n255\n", image->width, image->height);
  fwrite(image->data, sizeof(unsigned char), image->width * image->height, f);
}

image_t* velodyne32_depth_image_create(int width, int height) {
  image_t *image = (image_t*)calloc(1, sizeof(image_t));
  image->width = width;
  image->height = height;
  image->row_stride = width * sizeof(uint16_t);
  image->pixelformat = IMAGE_T_PIXEL_FORMAT_BE_GRAY16;
  image->size = height * image->row_stride;
  image->data = (uint8_t*)calloc(image->size, sizeof(uint8_t));
  image->nmetadata = 0;
  image->metadata = NULL;
  return image;
}

image_t* velodyne32_intensity_image_create(int width, int height) {
  image_t *image = (image_t*)calloc(1, sizeof(image_t));
  image->width = width;
  image->height = height;
  image->row_stride = width * sizeof(uint8_t);
  image->pixelformat = IMAGE_T_PIXEL_FORMAT_GRAY;
  image->size = height * image->row_stride;
  image->data = (uint8_t*)calloc(image->size, sizeof(uint8_t));
  image->nmetadata = 0;
  image->metadata = NULL;
  return image;
}
