# List all objects here, or use
LIB_$(D) = $(LIB_PATH)/libutils.a
LIBUTILS    	:= $(LIB_PATH)/libutils.a

LIB_SRC_$(D)	:= $(D)/image_utils.c $(D)/gl_utils.c
LIB_OBJS_$(D)	:= $(LIB_SRC_$(D):%.c=%.o)

OBJS_$(D)	:= 

# List all targets, it's important for building and cleaning
TGTS_$(D)	:= 

# List CFLAGS used when .o are compiled
$(LIB_OBJS_$(D)): 	CFLAGS_TGT := $(CFLAGS_APRIL)
$(OBJS_$(D)):   	CFLAGS_TGT := $(CFLAGS_APRIL) 

# List LDFlags and libs youn depend on here
$(TGTS_$(D)):   	LDFLAGS_TGT := $(LDFLAGS_LCM) $(LIBVX) $(LIBCOMMON)
$(TGTS_$(D)):   

$(LIB_$(D)): $(LIB_OBJS_$(D))
	@echo "$@"
	$(AR)

# If it doesn't make it into TGTS, it won't build
TGTS		:= $(TGTS) $(LIB_$(D)) $(TGTS_$(D)) 
CLEAN		:= $(CLEAN) $(LIB_$(D)) $(LIB_OBJS_$(D)) $(TGTS_$(D)) $(OBJS_$(D))
