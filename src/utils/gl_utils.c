/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <math.h>

#include <vx/vx.h>
#include <vx/vxo_lines.h>
#include <vx/vxo_matrix.h>

#include "gl_utils.h"

void save_snapshot(const vx_event_t *ev, const char *path) {
  image_u8x4_t *im = image_u8x4_create(ev->u.readpixels.width, ev->u.readpixels.height);
  for(int y = 0; y < ev->u.readpixels.height; y++) {
    memcpy(&im->buf[y*im->stride], &ev->u.readpixels.data[4*ev->u.readpixels.width*y], 4*ev->u.readpixels.width);
  }
  image_u8x4_write_pnm(im, path);
  image_u8x4_destroy(im);
}

void draw_cartesian_axes(vx_world_t *vw, vx_buffer_t *buffer, float length, float width) {
  float x[6] = { 0.0, 0.0, 0.0, length, 0.0, 0.0 };
  float y[6] = { 0.0, 0.0, 0.0, 0.0, length, 0.0 };
  float z[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, length };

  float x_axis_color[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
  vx_buffer_add_back(buffer,
		     vxo_lines(vx_resource_make_attr_f32_copy(x, 6, 3), 
			       x_axis_color, width), 
		     NULL);
  
  float y_axis_color[4] = { 0.0f, 1.0f, 0.0f, 1.0f };
  vx_buffer_add_back(buffer,
		     vxo_lines(vx_resource_make_attr_f32_copy(y, 6, 3), 
			       y_axis_color, width), 
		     NULL);
  
  float z_axis_color[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
  vx_buffer_add_back(buffer,
		     vxo_lines(vx_resource_make_attr_f32_copy(z, 6, 3), 
			       z_axis_color, width), 
		     NULL);
}

void draw_car_model(vx_world_t *vw, vx_buffer_t *buffer) {
  float car_color[4] = { 0.4, 0.4, 0.4, 1.0f };
  vx_buffer_add_back(buffer,
		     vxo_matrix_translate(0.0, 0.0, -1.8),
		     vxo_matrix_scale3(2.0, 1.5, 1.0), 
		     vxo_square_solid(car_color),
		     NULL);
  vx_buffer_add_back(buffer,
		     vxo_matrix_translate(0.0, 0.0, -1.3),
		     vxo_matrix_scale3(2.0, 1.5, 1.0), 
		     vxo_square_solid(car_color),
		     NULL);
  vx_buffer_add_back(buffer,
  		     vxo_matrix_translate(0.0, 0.75, -1.55),
  		     vxo_matrix_scale3(2.0, 1.0, 0.5),
  		     vxo_matrix_rotatex(M_PI / 2.0),
  		     vxo_square_solid(car_color),
  		     NULL);
  vx_buffer_add_back(buffer,
  		     vxo_matrix_translate(0.0, -0.75, -1.55),
  		     vxo_matrix_scale3(2.0, 1.0, 0.5),
  		     vxo_matrix_rotatex(M_PI / 2.0),
  		     vxo_square_solid(car_color),
  		     NULL);
  vx_buffer_add_back(buffer,
		     vxo_matrix_translate(1.0, 0.0, -1.55),
		     vxo_matrix_scale3(2.0, 1.5, 0.5), 
		     vxo_matrix_rotatey(M_PI / 2.0),
		     vxo_square_solid(car_color),
		     NULL);
  vx_buffer_add_back(buffer,
		     vxo_matrix_translate(-1.0, 0.0, -1.55),
		     vxo_matrix_scale3(2.0, 1.5, 0.5), 
		     vxo_matrix_rotatey(M_PI / 2.0),
		     vxo_square_solid(car_color),
		     NULL);
  zarray_t* lines_vertices = zarray_create(sizeof(float[3]));
  float origin[3] = { 0.0f, 0.0f, 0.0f };
  float front[3] = { 3.0, 0.0, 0.0f };
  zarray_add(lines_vertices, origin);
  zarray_add(lines_vertices, front);
  float direction_color[4] = { 0.0, 0.0, 0.0, 1.0f };
  vx_buffer_add_back(buffer,
		     vxo_matrix_translate(0.0, 0.0, -1.55),
		     vxo_lines(vx_resource_make_attr_f32_copy((float*)lines_vertices->data,
							      3 * zarray_size(lines_vertices),
							      3),
			       direction_color, 5),
		     NULL);
  zarray_destroy(lines_vertices);
}

