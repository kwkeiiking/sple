/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdio.h>
#include <stdlib.h>

#include <common/doubles.h>

#include "plane.h"

plane_t* plane_create_zeros() {
  plane_t *p = (plane_t*)calloc(1, sizeof(plane_t));
  p->origin = matd_create(4, 1);
  matd_put(p->origin, p->origin->nrows - 1, 0, 1.0f);
  p->normal = matd_create(4, 1);
  return p;
}

void plane_destroy(plane_t *to_destroy) {
  if(to_destroy == NULL) {
    return;
  }
  matd_destroy(to_destroy->origin);
  matd_destroy(to_destroy->normal);
  memset(to_destroy, 0, sizeof(plane_t));
  free(to_destroy);
}

void plane_print(plane_t *to_print) {
  printf("Origin = ");
  matd_print_transpose(to_print->origin, "%f ");
  printf("\n");
  printf("Normal = ");
  matd_print_transpose(to_print->normal, "%f ");
  printf("\n\n");
}

void plane_set_zeros(plane_t *p) {
  for(int i = 0; i < p->origin->nrows; ++i) {
    matd_put(p->origin, i, 0, 0.0);
    matd_put(p->normal, i, 0, 0.0);
  }
  matd_put(p->origin, p->normal->nrows - 1, 0, 1.0f);  
}

void plane_set_origin(plane_t *p, double *origin) {
  for(int i = 0; i < p->origin->nrows; ++i) {
    matd_put(p->origin, i, 0, origin[i]);
  }
}

void plane_set_normal(plane_t *p, double *normal) {
  for(int i = 0; i < p->normal->nrows; ++i) {
    matd_put(p->normal, i, 0, normal[i]);
  }
}

void plane_draw(plane_t *p, vx_world_t *vw, vx_buffer_t *buffer, double size) {
  double nworld[3] = { 0.0, 0.0, 1.0 };
  matd_t *nw = matd_create_data(3, 1, nworld);
  matd_t *np = matd_select(p->normal, 0, 2, 0, 0);
  matd_vec_normalize_inplace(np);
  matd_t *rotation_axis = matd_crossproduct(nw, np);
  double rotation_angle = acos(matd_vec_dot_product(nw, np));
  double aa_orientation[4] = { rotation_angle,
  			       rotation_axis->data[0], rotation_axis->data[1], rotation_axis->data[2] };
  double q_orientation[4];
  double rpy_orientation[3];
  doubles_angleaxis_to_quat(aa_orientation, q_orientation);
  doubles_quat_to_rpy(q_orientation, rpy_orientation);

  float plane_color[4] = { 1.0f, 0.6f, 0.2f, 1.0f };
  vx_buffer_add_back(buffer,
		     vxo_matrix_translate(p->origin->data[0], p->origin->data[1], p->origin->data[2]),
		     vxo_matrix_rotatez(rpy_orientation[2]),
		     vxo_matrix_rotatey(rpy_orientation[1]),
		     vxo_matrix_rotatex(rpy_orientation[0]),
		     vxo_matrix_scale3(size, size, 1.0), 
		     vxo_circle_solid((plane_color)),
		     NULL);

  matd_destroy(rotation_axis);
  matd_destroy(np);
  matd_destroy(nw);
}


double plane_point_distance_compute(plane_t *plane, matd_t *p) {
  // select non-homogeneous part of the vectors  
  matd_t *po = matd_select(plane->origin, 0, 2, 0, 0);
  matd_t *n = matd_select(plane->normal, 0, 2, 0, 0);
  matd_t *point = matd_select(p, 0, 2, 0, 0);
  
  matd_subtract_inplace(po, point);
  double dot = matd_vec_dot_product(po, n);
  double distance =  abs(dot) / matd_vec_mag(n);
  
  matd_destroy(po);
  matd_destroy(n);
  matd_destroy(point);

  return distance;
}

plane_t* plane_through3points_compute(matd_t *point1, matd_t *point2, matd_t *point3) {
  matd_t *p1 = matd_select(point1, 0, 2, 0, 0);
  matd_t *p2 = matd_select(point2, 0, 2, 0, 0);
  matd_t *p3 = matd_select(point3, 0, 2, 0, 0);

  matd_t *p12 = matd_subtract(p2, p1); 
  matd_t *p13 = matd_subtract(p3, p1);   
  matd_t *n = matd_crossproduct(p12, p13);

  plane_t *plane = plane_create_zeros();
  plane_set_origin(plane, p1->data);
  double normal[4] = { n->data[0], n->data[1], n->data[2], 0.0f };
  plane_set_normal(plane, normal);
  
  matd_destroy(p1);
  matd_destroy(p2);
  matd_destroy(p3);
  matd_destroy(p12);
  matd_destroy(p13);
  matd_destroy(n);
  
  return plane;
}

int plane_fitting_compute(plane_t** plane, point_cloud_t *point_cloud, zarray_t *data,
			  double eigenvalues_threshold) {
  int good_fitting_plane = 0;
  int data_size = zarray_size(data);
  point_accumulator_t *pa = point_accumulator_create();  
  for(int i = 0; i < data_size; ++i) {
    int *index;
    zarray_get_volatile(data, i, &index);
    rich_point_t *rp;
    zarray_get_volatile(point_cloud->measurements, *index, &rp);
    point_accumulator_add_point_inplace(pa, rp->point);
  }

  *plane = plane_create_zeros();
  matd_t *origin = point_accumulator_mean(pa);
  matd_t *covariance = point_accumulator_covariance(pa);
  matd_svd_t svd = matd_svd_flags(covariance, 1);
  matd_t *v1 = matd_select(svd.U, 0, 2, 0, 0);
  matd_t *v2 = matd_select(svd.U, 0, 2, 1, 1);
  matd_t *n = matd_crossproduct(v1, v2);
  double ratio = (matd_get(svd.S, 2, 2) / 
		  (matd_get(svd.S, 2, 2) + matd_get(svd.S, 1, 1) + matd_get(svd.S, 0, 0)));
  if(ratio > eigenvalues_threshold) {
    plane_destroy(*plane);
    *plane = NULL;
  }
  else {
    plane_set_origin(*plane, origin->data);
    double normal[4] = { n->data[0], n->data[1], n->data[2], 0.0f };
    plane_set_normal(*plane, normal);
    good_fitting_plane = 1;
  }

  matd_destroy(n);
  matd_destroy(v2);
  matd_destroy(v1);
  matd_destroy(svd.U);
  matd_destroy(svd.S);
  matd_destroy(svd.V);
  matd_destroy(covariance);
  matd_destroy(origin);
  point_accumulator_destroy(pa);

  return good_fitting_plane;
}

double plane_fitting_error_compute(plane_t *p, point_cloud_t *point_cloud, zarray_t *data) {
  double error = 0.0;
  int data_size = zarray_size(data);  
  for(int i = 0; i < data_size; ++i) {
    int *index;
    zarray_get_volatile(data, i, &index);
    rich_point_t *rp;
    zarray_get_volatile(point_cloud->measurements, *index, &rp);
    error += plane_point_distance_compute(p, rp->point);
  }
  return error / data_size;
}

