/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#ifndef CLUSTERIZER_H
#define CLUSTERIZER_H

#include <common/zqueue.h>

#include "point_cloud.h"

struct pixel {
  int row;
  int col;
  int index;
};
typedef struct pixel pixel_t;

int zqueue_contains_pixel(zqueue_t *zq, pixel_t *pixel);

void cluster_draw(zarray_t *cluster, point_cloud_t *point_cloud, 
		  vx_buffer_t *buffer, int point_size);

zarray_t* cluster_point_compute(point_cloud_t *point_cloud, mati_t *indices,
				int min_cluster_dimension, int neighbouring_radius,
				double point_distance_threshold, double point_min_range);

zarray_t* cluster_point_and_normal_compute(point_cloud_t *point_cloud, mati_t *indices,
					   int min_cluster_dimension, int neighbouring_radius, 
					   double point_distance_threshold, double point_min_range, 
					   double normal_angle_threshold);

void cluster_fitting_lines_planes_compute(zarray_t **lines, zarray_t **planes, 
					  zarray_t *clusters, point_cloud_t *point_cloud, 
					  double line_max_error, double plane_max_error, 
					  double line_max_eigenvalues_ratio, 
					  double plane_max_eigenvalues_ratio,
					  double line_min_length,
					  int plane_min_points);

#endif
