/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include <lcm/lcm.h>

#include <common/time_util.h>
#include <common/zarray.h>

#include <lcmtypes/pose_t.h>
#include "lcmtypes/velodyne_features_t.h"

struct state {
  // lcm fields
  lcm_t *lcm;

  // data fields
  pose_t *pose; // [pos | orientation] = [tx ty tz | qw qx qy qz] 
  FILE *fp;  

  // synchronization fields
  pthread_mutex_t pose_lock;
};
typedef struct state state_t;

static void pose_callback(const lcm_recv_buf_t *rbuf, const char *channel,
			  const pose_t *msg, void *user) {
  state_t *state = (state_t*)user;
  pthread_mutex_lock(&state->pose_lock);
  if(state->pose) {
    pose_t_destroy(state->pose);
  }
  state->pose = pose_t_copy(msg);
  pthread_mutex_unlock(&state->pose_lock);
}

static void velodyne_features_callback(const lcm_recv_buf_t *rbuf, const char *channel,
				       const velodyne_features_t *msg, void *user) {
  state_t *state = (state_t*)user;

  pthread_mutex_lock(&state->pose_lock);
  if(state->pose) {
    // write vertex
    // [tx ty tz qw qx qy qz]
    fprintf(state->fp, "\nvertex %f %f %f %f %f %f %f\n",
  	    state->pose->pos[0], state->pose->pos[1], state->pose->pos[2],
  	    state->pose->orientation[0], state->pose->orientation[1],
  	    state->pose->orientation[2], state->pose->orientation[3]);
    pthread_mutex_unlock(&state->pose_lock);        

    velodyne_features_t* features = velodyne_features_t_copy(msg);
    
    // write lines
    // [origin_x origin_y origin_z direction_x direction_y direction_z]
    double *lines_data = features->lines;
    for(int i = 0; i < features->lines_data_length / 6; ++i) {
      fprintf(state->fp, "line %f %f %f %f %f %f\n",
	      lines_data[0], lines_data[1], lines_data[2],
	      lines_data[3], lines_data[4], lines_data[5]);
      lines_data += 6;
    }

    // write planes
    // [origin_x origin_y origin_z normal_x normal_y normal_z]		
    double *planes_data = features->planes;
    for(int i = 0; i < features->planes_data_length / 6; ++i) {
      fprintf(state->fp, "plane %f %f %f %f %f %f\n",
	      planes_data[0], planes_data[1], planes_data[2],
	      planes_data[3], planes_data[4], planes_data[5]);
      planes_data += 6;
    }

    printf(".");
    fflush(stdout);
    fflush(state->fp);
    velodyne_features_t_destroy(features);
  }
  else {
    pthread_mutex_unlock(&state->pose_lock);  
  }
}

void init_state(state_t *state, char *filename) {
  state->lcm = lcm_create(NULL);
  if(!state->lcm) {
    printf("[ERROR] impossible to initialize LCM environment... quitting!");
    exit(-1);
  }

  if((state->fp = fopen(filename, "w")) == NULL) { 
    printf("[ERROR] impossible to open dump file %s... quitting!", filename);
    exit(-1);
  }
  fprintf(state->fp, "# vertex [tx ty tz qw qx qy qz]\n");
  fprintf(state->fp, "# line [origin_x origin_y origin_z direction_x direction_y direction_z]\n");
  fprintf(state->fp, "# plane [origin_x origin_y origin_z normal_x normal_y normal_z]\n");
  fflush(state->fp);

  state->pose = NULL;
}

int main(int argc, char **argv) {  
  // handle input
  printf("[INFO] usage: message_dumper dump_filename.txt]\n");
  printf("[INFO] \texample --> message_dumper dump.txt\n");
  
  if(argc < 2) {
    printf("[ERROR] missing dump file name... quitting!\n");
    return 0;
  }

  // init state
  state_t *state = calloc(1, sizeof(state_t));
  init_state(state, argv[1]);

  // subscribe to velodyne data channel
  velodyne_features_t_subscribe(state->lcm, "VELODYNE_FEATURES", 
				&velodyne_features_callback, state);
  pose_t_subscribe(state->lcm, "POSE", &pose_callback, state);

  // run the loop
  while(1) {					
    lcm_handle(state->lcm);
  }

  pthread_mutex_lock(&state->pose_lock);
  if(state->pose) {
    pose_t_destroy(state->pose);
  }
  pthread_mutex_unlock(&state->pose_lock);
  fclose(state->fp);
  lcm_destroy(state->lcm);
  free(state);

  return 0;
}
