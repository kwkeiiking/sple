# List all objects here, or use
OBJS_$(D)	:= $(D)/point_accumulator_test.o $(D)/integral_image_test.o \
	$(D)/line_fitting_test.o $(D)/plane_fitting_test.o $(D)/message_dumper.o

# List all targets, it's important for building and cleaning
TGTS_$(D)	:= $(D)/point-accumulator-test $(D)/integral-image-test $(D)/line-fitting-test \
	$(D)/plane-fitting-test $(D)/message-dumper

# List CFLAGS used when .o are compiled
$(OBJS_$(D)):   	CFLAGS_TGT := $(CFLAGS_APRIL) 

# List LDFlags and libs youn depend on here
$(TGTS_$(D)):   	LDFLAGS_TGT := $(LDFLAGS_LCM) $(LIBVX) $(LIBEK) $(LIBHTTPD) $(LIBCOMMON) $(LIBLCMTYPES)
$(TGTS_$(D)):   	$(LIBVELODYNE32FEATURES) $(LIBUTILS) 

$(D)/point-accumulator-test:	$(D)/point_accumulator_test.o
	$(LINK)

$(D)/integral-image-test:	$(D)/integral_image_test.o
	$(LINK)

$(D)/line-fitting-test:	$(D)/line_fitting_test.o
	$(LINK)

$(D)/plane-fitting-test:	$(D)/plane_fitting_test.o
	$(LINK)

$(D)/message-dumper:	$(D)/message_dumper.o
	$(LINK)

# If it doesn't make it into TGTS, it won't build
TGTS		:= $(TGTS) $(TGTS_$(D))
CLEAN		:= $(CLEAN) $(TGTS_$(D)) $(OBJS_$(D))
