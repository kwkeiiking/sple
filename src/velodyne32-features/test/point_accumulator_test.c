/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdio.h>

#include "velodyne32-features/point_accumulator.h"
#include "velodyne32-features/point_cloud.h"

int main(int argc, char **argv) {
  point_accumulator_t *pa =  point_accumulator_create();

  int rnd;
  for(int i = 0; i < 1000;  ++i) {
    rich_point_t* rp = rich_point_create_zeros();
    rnd = rand() % 1000;
    double x = 0.001 * (double)rnd; 
    rnd = rand() % 1000;
    double y = 0.001 * (double)rnd; 
    rnd = rand() % 1000;
    double z = 0.001 * (double)rnd; 
    double point[4] = { x, y, z, 1.0 }; 
    rich_point_set_point(rp, point);
    point_accumulator_add_point_inplace(pa, rp->point);
    rich_point_destroy(rp);
  }

  matd_t *mean = point_accumulator_mean(pa);
  printf("Mean =\n");
  matd_print(mean, "%f ");
  printf("\n");

  matd_t *covariance = point_accumulator_covariance(pa);
  printf("Covariance =\n");
  matd_print(covariance, "%f ");
  printf("\n");

  point_accumulator_destroy(pa);
  matd_destroy(mean);
  matd_destroy(covariance);

  return 0;
}
