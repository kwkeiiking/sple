/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdio.h>

#include "velodyne32-features/integral_image.h"
#include "velodyne32-features/point_cloud.h"

#define ROWS 10
#define COLS 20

int main(int argc, char **argv) {
  mati_t *indices = mati_create(ROWS, COLS);
  point_cloud_t * point_cloud = point_cloud_create(sizeof(rich_point_t));
  point_cloud_pre_allocate_structures(point_cloud, COLS, ROWS);
  
  int index = 0;
  for(int r = 0; r < indices->nrows; ++r) {
    for(int c = 0; c < indices->ncols; ++c) {
      double point[4] = { r * c, r * c, r * c, 1.0 };      
      rich_point_t *rich_point; 
      printf("Index: %d\n", index);
      fflush(stdout);
      zarray_get_volatile(point_cloud->measurements, index, &rich_point);        
      rich_point_set_point(rich_point, point);

      if(r * c == 0) {
	mati_put(indices, r, c, -1);
      }
      else {
	mati_put(indices, r, c, index);
      }
      index++;
    }
  }
  printf("Source matrix:\n");
  index = 0;
  for(int r = 0; r < indices->nrows; ++r) {
    for(int c = 0; c < indices->ncols; ++c) {
      rich_point_t *rich_point; 
      zarray_get_volatile(point_cloud->measurements, index, &rich_point);        
      printf("%.1f\t", matd_get(rich_point->point, 0, 0));
      index++;
    }
    printf("\n");
  }

  integral_image_t *ii = integral_image_create(20, 10);
  printf("Initial integral image:\n");
  for(int r = 0; r < ii->height; ++r) {
    for(int c = 0; c < ii->width; ++c) {
      point_accumulator_t *pa = ii->buf + r * ii->width + c;        
      printf("%.1f\t", matd_get(pa->sum, 0, 0));
    }
    printf("\n");
  }

  integral_image_point_compute(ii, indices, point_cloud);
  printf("Integral image after computation:\n");
  for(int r = 0; r < ii->height; ++r) {
    for(int c = 0; c < ii->width; ++c) {
      point_accumulator_t *pa = ii->buf + r * ii->width + c;        
      printf("%.1f\t", matd_get(pa->sum, 0, 0));
    }
    printf("\n");
  }

  int row_image_radius = 1;
  int col_image_radius = 2;
  int r = 3;
  int c = 3;
  point_accumulator_t *pa_r1 = integral_image_get_region(ii,
							 r - row_image_radius, r + row_image_radius, 
							 c - col_image_radius, c + col_image_radius);
  point_accumulator_print(pa_r1);

  integral_image_clear(ii);
  integral_image_destroy(ii);
  
  return 0;
}
