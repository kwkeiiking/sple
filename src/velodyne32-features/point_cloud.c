/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <omp.h>
#include <stdio.h>

#include <vx/vxo_generic.h>

#include "point_cloud.h"

rich_point_t* rich_point_create_zeros() {
  rich_point_t *rp = (rich_point_t*)calloc(1, sizeof(rich_point_t));
  rp->point = matd_create(4, 1);
  matd_put(rp->point, rp->point->nrows - 1, 0, 1.0f);
  rp->normal = matd_create(4, 1);
  return rp;
}

void rich_point_destroy(rich_point_t *to_destroy) {
  if(to_destroy == NULL) {
    return;
  }
  matd_destroy(to_destroy->point);
  matd_destroy(to_destroy->normal);
  memset(to_destroy, 0, sizeof(rich_point_t));
  free(to_destroy);
}

void rich_point_print(rich_point_t *to_print) {
  printf("Point = ");
  matd_print_transpose(to_print->point, "%f ");
  printf("\n");
  printf("Normal = ");
  matd_print_transpose(to_print->normal, "%f ");
  printf("\n");
  printf("Curvature = %f\n\n", to_print->curvature);
}

void rich_point_set_zeros(rich_point_t *rp) {
  for(int i = 0; i < rp->point->nrows; ++i) {
    matd_put(rp->point, i, 0, 0.0);
    matd_put(rp->normal, i, 0, 0.0);
  }
  matd_put(rp->point, rp->point->nrows - 1, 0, 1.0f);
  rp->curvature = 0.0;
}

void rich_point_set_point(rich_point_t *rp, double* point) {
  for(int i = 0; i < rp->point->nrows; ++i) {
    matd_put(rp->point, i, 0, point[i]);
  }
}

void rich_point_set_normal(rich_point_t *rp, double* normal){
  for(int i = 0; i < rp->normal->nrows; ++i) {
    matd_put(rp->normal, i, 0, normal[i]);
  }
}

point_cloud_t* point_cloud_create(size_t element_size) {
  assert(element_size > 0);

  point_cloud_t *pc = (point_cloud_t*)calloc(1, sizeof(point_cloud_t));
  pc->measurements = zarray_create(element_size);
  return pc;
}

void point_cloud_destroy(point_cloud_t *to_destroy) {
  if(to_destroy == NULL) {
    return;
  }

  if(to_destroy->measurements != NULL) {
    for(int i = 0; i < zarray_size(to_destroy->measurements); ++i) {
      rich_point_t *rp;
      zarray_get_volatile(to_destroy->measurements, i, &rp);
      matd_destroy(rp->point);
      matd_destroy(rp->normal);
    }
    zarray_destroy(to_destroy->measurements);  
  }
  memset(to_destroy, 0, sizeof(point_cloud_t));
  free(to_destroy);
}

void point_cloud_pre_allocate_structures(point_cloud_t *point_cloud, int width, int height) {
  int size = width * height;
  zarray_ensure_capacity(point_cloud->measurements, size);
  for(int i = 0; i < size; ++i) {
    rich_point_t *rp = rich_point_create_zeros();
    zarray_add(point_cloud->measurements, rp);
    free(rp);
  }
} 

void point_cloud_compute_normals(point_cloud_t *point_cloud, 
				 integral_image_t *integral_image, mati_t *indices,
				 int row_image_radius, int col_image_radius, int min_points) {
  assert(zarray_size(point_cloud->measurements));
  assert(indices->ncols > 0 && indices->nrows > 0);    
  
#pragma omp parallel for
  for(int r = 0; r < indices->nrows; ++r) {
    int32_t *index = indices->data + r * indices->ncols;
    for(int c = 0; c < indices->ncols; ++c, ++index) {
      if(*index < 0) { 
	continue; 
      }

      point_accumulator_t* pa = integral_image_get_region(integral_image,
							  r - row_image_radius, r + row_image_radius, 
							  c - col_image_radius, c + col_image_radius);
      if(point_accumulator_n(pa) < min_points) { 
	point_accumulator_destroy(pa);
	*index = -1;
	continue; 
      }

      rich_point_t* rp;
      zarray_get_volatile(point_cloud->measurements, *index, &rp);
      matd_t *covariance = point_accumulator_covariance(pa);
      matd_svd_t svd = matd_svd_flags(covariance, 1);      
      double normal[4] = { matd_get(svd.U, 0, 2), matd_get(svd.U, 1, 2), matd_get(svd.U, 2, 2), 
			   0.0f };
      rich_point_set_normal(rp, normal);
      rp->curvature = matd_get(svd.S, 2, 2) / (matd_get(svd.S, 0, 0) + 
					       matd_get(svd.S, 1, 1) + 
					       matd_get(svd.S, 2, 2));
      
      if(!matd_vec_normalize_inplace(rp->normal)) {
	double zero[4] = { 0.0, 0.0, 0.0, 0.0f };
	rich_point_set_normal(rp, zero);
	rp->curvature = 0.0;
	*index = -1;
      }
      else {
	if(matd_vec_dot_product(rp->normal, rp->point) > 0.0) {
	  matd_scale_inplace(rp->normal, -1.0);
	}
      }            
             
      matd_destroy(svd.U);
      matd_destroy(svd.S);
      matd_destroy(svd.V);
      matd_destroy(covariance);
      point_accumulator_destroy(pa);
    }
  }
}

void point_cloud_compute_normals_using_intervals(point_cloud_t *point_cloud, 
						 integral_image_t *integral_image, 
						 mati_t *indices, interval_image_t *intervals,
						 int min_points) {
  assert(zarray_size(point_cloud->measurements));
  assert(intervals->width > 0 && intervals->height > 0);   
  assert(indices->ncols == intervals->width && indices->nrows == intervals->height);    
  
#pragma omp parallel for
  for(int r = 0; r < indices->nrows; ++r) {
    int32_t *index = indices->data + r * indices->ncols;
    interval_t *interval = intervals->buf + r * intervals->width;
    for(int c = 0; c < indices->ncols; ++c, ++index, ++interval) {
      if(*index < 0) { 
	continue; 
      }

      int left_col = interval->left_col < 0 ? 0 : interval->left_col;      
      int right_col = interval->right_col < 0 ? 0 : interval->right_col;      
      int up_row = interval->up_row < 0 ? 0 : interval->up_row;      
      int bottom_row = interval->bottom_row < 0 ? 0 : interval->bottom_row;      
      if(interval->left_col < 0 || interval->right_col < 0 ||
      	 interval->up_row < 0 || interval->bottom_row < 0) {
	*index = -1;
	continue;
      }

      point_accumulator_t *pa = integral_image_get_region(integral_image,
							  r - up_row, r + bottom_row, 
							  c - left_col, c + right_col);
      if(point_accumulator_n(pa) < min_points) { 
	point_accumulator_destroy(pa);
	*index = -1;
	continue; 
      }

      rich_point_t *rp;
      zarray_get_volatile(point_cloud->measurements, *index, &rp);
      matd_t *covariance = point_accumulator_covariance(pa);
      matd_svd_t svd =  matd_svd_flags(covariance, 1);      
      double normal[4] = { matd_get(svd.U, 0, 2), matd_get(svd.U, 1, 2), matd_get(svd.U, 2, 2), 
			   0.0f };
      rich_point_set_normal(rp, normal);
      rp->curvature = matd_get(svd.S, 2, 2) / (matd_get(svd.S, 0, 0) + 
					       matd_get(svd.S, 1, 1) + 
					       matd_get(svd.S, 2, 2));
      
      if(!matd_vec_normalize_inplace(rp->normal)) {
	double zero[4] = { 0.0, 0.0, 0.0, 0.0f };
	rich_point_set_normal(rp, zero);
	rp->curvature = 0.0;
	*index = -1;
      }
      else {
	if(matd_vec_dot_product(rp->normal, rp->point) > 0.0) {
	  matd_scale_inplace(rp->normal, -1.0);
	}
      }            

      matd_destroy(svd.U);
      matd_destroy(svd.S);
      matd_destroy(svd.V);
      matd_destroy(covariance);
      point_accumulator_destroy(pa);
    }
  }
}

void point_cloud_draw(point_cloud_t *point_cloud, vx_world_t *vw,
		      char* point_buffer, float *point_color, 
		      int point_size, double normal_scale, 
		      double curvature_threshold) {
  int cloud_size = zarray_size(point_cloud->measurements);
  zarray_t *points = zarray_create(sizeof(float[3]));
  zarray_t *normals = zarray_create(sizeof(float[3]));
  zarray_t *curvature_thresholded_points = zarray_create(sizeof(float[3]));
  zarray_ensure_capacity(points, cloud_size);
  zarray_ensure_capacity(normals, cloud_size);
  zarray_ensure_capacity(curvature_thresholded_points, cloud_size);  
  for(int i = 0; i < cloud_size; ++i)      {
    rich_point_t *rp;
    zarray_get_volatile(point_cloud->measurements, i, &rp);
    double range = matd_vec_mag(rp->point);
    if(range == 1.0) {
      continue;
    }    

    // Point
    float p[3] = { matd_get(rp->point, 0, 0), 
		   matd_get(rp->point, 1, 0), 
		   matd_get(rp->point, 2, 0) };
    zarray_add(points, p);

    double normal_length = matd_vec_mag(rp->normal);
    if(!(normal_length > 0.0)) {
      continue;
    }    

    // Point curvature
    double c = rp->curvature;
    if(!(c > curvature_threshold)) {
      zarray_add(curvature_thresholded_points, p);
    }

    // Normal
    float n[3] = { p[0] + matd_get(rp->normal, 0, 0) * normal_scale, 
		   p[1] + matd_get(rp->normal, 1, 0) * normal_scale, 
		   p[2] + matd_get(rp->normal, 2, 0) * normal_scale };    
    zarray_add(normals, p);
    zarray_add(normals, n);
  }

  // Draw points
  if(1) {
    vx_buffer_t *vb = vx_world_get_buffer(vw, point_buffer);
    vx_buffer_add_back(vb,
  		       vxo_points(vx_resource_make_attr_f32_copy((float*)points->data,
								 3 * zarray_size(points), 3), 
				  point_color, point_size),
  		       NULL);
    vx_buffer_swap(vb);
  }

  // Draw points based on curvature
  if(0) {
    vx_buffer_t *vb = vx_world_get_buffer(vw, "curvature_thresholded_points");
    float point_color[4] = { 1.0f, 0.6f, 0.0f, 1.0f };
    vx_buffer_add_back(vb,
  		       vxo_points(vx_resource_make_attr_f32_copy((float*)curvature_thresholded_points->data,
								 3 * zarray_size(curvature_thresholded_points), 3), 
				  point_color, point_size),
  		       NULL);
    vx_buffer_swap(vb);
  }

  // Draw normals
  if(0) {
    vx_buffer_t *vb = vx_world_get_buffer(vw, "surface_normals");
    float normal_color[4] = { 0.0f, 0.47f, 0.44f, 1.0f };
    vx_buffer_add_back(vb,
		       vxo_lines(vx_resource_make_attr_f32_copy((float*)normals->data,
								3 * zarray_size(normals), 3), 
				 normal_color, 3.0),
		       NULL);
    vx_buffer_swap(vb);
  }

  zarray_destroy(curvature_thresholded_points);
  zarray_destroy(points);
  zarray_destroy(normals);
}

vx_object_t *vxo_points_with_intensities(vx_resource_t *verts, vx_resource_t *intensities, 
					 vx_resource_t *intensity_scale, 
					 char* frag_color_instruction) {
  static vx_resource_t *program_resource = NULL;

  vx_lock();
  if(program_resource == NULL) {
    char *vertex_shader_src =
      "attribute vec3 position; \n"                  \
      "attribute float intensity; \n"                \
      "attribute float intensity_scale; \n"          \
      "uniform mat4 VX_P;\n"                         \
      "uniform mat4 VX_V;\n"                         \
      "uniform mat4 VX_M;\n"                         \
      "varying vec4 vposition; \n"                   \
      "varying float vintensity;\n"                  \
      "varying float vintensity_scale;\n"            \
      "void main(void) {\n"                          \
      "  vposition = VX_M * vec4(position, 1.0);\n " \
      "  gl_Position = VX_P * VX_V * vposition;\n "  \
      "  gl_PointSize = min(3.0, gl_Position.z);\n"  \
      "  vintensity = intensity; \n"                 \
      "  vintensity_scale = intensity_scale; \n"     \
      "}";

    char fragment_shader_src[10000];
    strcat(fragment_shader_src, "precision mediump float; \n");
    strcat(fragment_shader_src, "varying vec4 vposition; \n");
    strcat(fragment_shader_src, "varying float vintensity; \n");
    strcat(fragment_shader_src, "varying float vintensity_scale; \n");
    strcat(fragment_shader_src, "void main(void) {\n");
    strcat(fragment_shader_src, frag_color_instruction);
    strcat(fragment_shader_src, "}\n");

    program_resource = vx_resource_make_program(vertex_shader_src, fragment_shader_src);
    program_resource->incref(program_resource);
  }
  vx_unlock();

  return vxo_generic_create(program_resource,
			    (struct vxo_generic_uniformf[]) {
			      { .name=NULL } },
			    (struct vxo_generic_attribute[]) {
			      { .name="position", .resource = verts },
				{ .name="intensity", .resource = intensities },
				  { .name="intensity_scale", .resource = intensity_scale },
				    { .name=NULL } },
			    (struct vxo_generic_texture[]) {
			      {.name=NULL } },
			    (struct vxo_generic_draw[]) {
			      { .command = VX_GL_POINTS, .first = 0, .count = verts->u.attr_f32.nelements / verts->u.attr_f32.dim },
				{ .count = 0 }, });
}
