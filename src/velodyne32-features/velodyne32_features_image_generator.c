/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#include <lcm/lcm.h>

#include <common/zarray.h>
#include <common/zqueue.h>

#include <lcmtypes/image_t.h>
#include <lcmtypes/raw_t.h>

#include <vx/vx.h>
#include <vx/webvx.h>

#include "utils/gl_utils.h"
#include "utils/image_utils.h"

#define MIN_RANGE 2.5
#define MAX_RANGE 65
#define WIDTH 2100
#define HEIGHT 32

struct state {
  // lcm fields
  lcm_t *lcm;

  // viewer fields
  vx_world_t *vw;
  webvx_t *webvx;

  // velodyne fields
  double *vertical_angle_sincos;
  zqueue_t *velodyne_msgs;

  // synch fields
  pthread_mutex_t velodyne_msgs_mutex;
};
typedef struct state state_t;

double vertical_angle_degrees[32] = {
  -30.67, -9.33,  -29.33, -8.00,
  -28.00, -6.66,  -26.66, -5.33,
  -25.33, -4.00,  -24.00, -2.67,
  -22.67, -1.33,  -21.33,  0.00,
  -20.00,  1.33,  -18.67,  2.67,
  -17.33,  4.00,  -16.00,  5.33,
  -14.67,  6.67,  -13.33,  8.00,
  -12.00,  9.33,  -10.67, 10.67
};

int vertical_angle_indexes[32] = {
  31, 15,  30, 14,
  29, 13,  28, 12,
  27, 11,  26, 10,
  25,  9,  24,  8,
  23,  7,  22,  6,
  21,  5,  20,  4,
  19,  3,  18,  2,
  17,  1,  16,  0
};

void on_create_canvas(vx_canvas_t *vc, const char *name, void *impl) {
  state_t *state = (state_t*)impl;

  printf("[INFO] on create canvas\n");
  vx_layer_t *vl = vx_canvas_get_layer(vc, "default");

  vx_layer_set_world(vl, state->vw);
  float background_color[4] = { 0.95, 0.95, 0.96, 1.0 };
  vx_layer_set_background_rgba(vl, background_color, 0);
}

void on_destroy_canvas(vx_canvas_t *vc, void *impl) {
  state_t *state = (state_t*)impl;
  printf("[INFO] on destroy canvas\n");
}

static void velodyne_callback(const lcm_recv_buf_t *rbuf, const char *channel,
                              const raw_t *msg, void *user) {
  // add velodyne msgs to the processing queue
  state_t *state = (state_t*)user;
  raw_t *msg_copy = raw_t_copy(msg);
  pthread_mutex_lock(&state->velodyne_msgs_mutex);
  zqueue_push(state->velodyne_msgs, &msg_copy);
  pthread_mutex_unlock(&state->velodyne_msgs_mutex);
}

void* process_velodyne_data(void* arg) {
  state_t *state = (state_t*)arg;
  double theta_start = 0.0;
  double theta_offset = 2.0 * M_PI / WIDTH;
  double theta_factor = -2.0 * M_PI / 36000.0;
  zarray_t *points = zarray_create(sizeof(float[3]));
  zarray_t *intensities = zarray_create(sizeof(float));
  image_t *depth_image = velodyne32_depth_image_create(WIDTH, HEIGHT);
  image_t *intensity_image = velodyne32_intensity_image_create(WIDTH, HEIGHT);    
  while(1) {
    pthread_mutex_lock(&state->velodyne_msgs_mutex);
    if(zqueue_size(state->velodyne_msgs) == 0) {
      pthread_mutex_unlock(&state->velodyne_msgs_mutex);
      usleep(100);
      continue;
    }
    raw_t *msg = 0;
    zqueue_pop(state->velodyne_msgs, &msg);
    pthread_mutex_unlock(&state->velodyne_msgs_mutex);

    // check if data length is correct
    if(msg->len != 1206) {
      printf("[WARNING] received a message with bad data length %d... skipping\n", msg->len);
      continue;
    }

    // process points
    for(int blocks = 0; blocks < 12; ++blocks) {
      const uint8_t* data = &msg->buf[100 * blocks];
      uint32_t block = (data[0] << 0) + (data[1] << 8);
      if(block != 0xeeff && block != 0xddff) {
	printf("[WARNING] encountered bad block %d... skipping\n", block);
	continue;
      }

      double theta = theta_factor * ((data[2] << 0) + (data[3] << 8));
      double ctheta = cos(theta);
      double stheta = sin(theta);
      uint8_t* intensity_image_data = (uint8_t*)intensity_image->data; 
      uint16_t* depth_image_data = (uint16_t*)depth_image->data; 
      int column = fabs(theta) / theta_offset;
      if(column >= WIDTH) { 
	column = 0;
      }
      for(int laseridx = 0; laseridx < 32; ++laseridx) {
	int row = vertical_angle_indexes[laseridx];
	double range = ((data[4 + laseridx * 3 + 0] << 0) +
		       (data[4 + laseridx * 3 + 1] << 8)) * 0.002;
	uint8_t intensity = data[4 + laseridx * 3 + 2];
	if(range < MIN_RANGE || range > MAX_RANGE) {
	  continue;
	}
	double cpsi = state->vertical_angle_sincos[2 * laseridx + 1];
	double spsi = state->vertical_angle_sincos[2 * laseridx + 0];
	double horiz = cpsi * range;
	double x = ctheta * horiz;
	double y = stheta * horiz;
	double z = spsi * range;

	float xyz[] = { x, y, z };
	zarray_add(points, xyz);

	float fintensity = intensity / 255.0;
	zarray_add(intensities, &fintensity);
	
	intensity_image_data[row * WIDTH + column] = intensity;
	depth_image_data[row * WIDTH + column] = (uint16_t)(range * 1000.0);
      }

      if(theta_start > theta) {
	theta_start = theta;
      } else {
	vx_buffer_t* vb = vx_world_get_buffer(state->vw, "points");
	float points_color[4] = { 0.03f, 0.27f, 0.49f, 1.0f };
	vx_buffer_add_back(vb,
			   vxo_points(vx_resource_make_attr_f32_copy((float*)points->data,
								     3 * zarray_size(points), 3),
				      points_color, 3),
			   NULL);
	vx_buffer_swap(vb);

	depth_image->utime = msg->utime;
	intensity_image->utime = msg->utime;
	image_t_publish(state->lcm, "VELODYNE_DEPTH_IMAGE", depth_image);
	// image_t_publish(state->lcm, "VELODYNE_INTENSITY_IMAGE", intensity_image);

	zarray_destroy(points);
	zarray_destroy(intensities);
	image_t_destroy(depth_image);
	image_t_destroy(intensity_image);

	points = zarray_create(sizeof(float[3]));
	intensities = zarray_create(sizeof(float));
	depth_image = velodyne32_depth_image_create(WIDTH, HEIGHT);
	intensity_image = velodyne32_intensity_image_create(WIDTH, HEIGHT);    

	theta_start = 0.0;
      }
    }

    raw_t_destroy(msg);
  }

  return NULL;
}

void init_state(state_t *state) {
  state->lcm = lcm_create(NULL);
  if(!state->lcm) {
    printf("[ERROR] impossible to initialize LCM environment... quitting!");
    exit(-1);
  }

  state->vw = vx_world_create();
  state->webvx = webvx_create_server(2387, NULL, "index.html");
  printf("[INFO] viewer enabled on port 2387\n");
  webvx_define_canvas(state->webvx, "velodyne32_image_generator_viewer_canvas",
		      on_create_canvas, on_destroy_canvas, state);
  vx_buffer_t *vb_cartesian_axes = vx_world_get_buffer(state->vw, "cartesian_axes");  
  draw_cartesian_axes(state->vw, vb_cartesian_axes, 1.0, 5.0);
  vx_buffer_swap(vb_cartesian_axes);
  vx_buffer_t *vb_car_model = vx_world_get_buffer(state->vw, "car_model");  
  draw_car_model(state->vw, vb_car_model);
  vx_buffer_swap(vb_car_model);

  state->vertical_angle_sincos = malloc(32 * 2 * sizeof(double));
  for(int i = 0; i < 32; ++i) {
    double rad = vertical_angle_degrees[i] * M_PI / 180.0;
    state->vertical_angle_sincos[2 * i + 0] = sin(rad);
    state->vertical_angle_sincos[2 * i + 1] = cos(rad);
  }

  state->velodyne_msgs = zqueue_create(sizeof(raw_t*));
}

int main(int argc, char **argv) {
  // init state
  state_t *state = calloc(1, sizeof(state_t));
  init_state(state);

  // create thread that processes velodyne point clouds
  pthread_t tid;
  int err = pthread_create(&tid, NULL, &process_velodyne_data, state);
  if(err != 0) {
    printf("[ERROR] can't create thread to process velodyne point clouds [%s]\n",
	   strerror(err));
    return -1;
  }

  // subscribe to velodyne data channel
  raw_t_subscribe(state->lcm, "VELODYNE_DATA", &velodyne_callback, state);

  // run the loop
  while(1) {
    lcm_handle(state->lcm);
  }

  free(state->vertical_angle_sincos);
  zqueue_destroy(state->velodyne_msgs);
  lcm_destroy(state->lcm);
  free(state);

  return 0;
}
