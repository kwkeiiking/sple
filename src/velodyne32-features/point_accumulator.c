/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdlib.h>
#include <stdio.h>

#include <omp.h>

#include "point_accumulator.h"

point_accumulator_t* point_accumulator_create() {
  point_accumulator_t *pa = (point_accumulator_t*)calloc(1, sizeof(point_accumulator_t));
  pa->sum = matd_create(4, 1);
  pa->squared_sum = matd_create(4, 4);
  return pa;
}

void point_accumulator_destroy(point_accumulator_t *to_destroy) {
  if(to_destroy == NULL) {
    return;
  }
  matd_destroy(to_destroy->sum);
  matd_destroy(to_destroy->squared_sum);
  memset(to_destroy, 0, sizeof(point_accumulator_t));
  free(to_destroy);
}

void point_accumulator_print(point_accumulator_t *to_print) {
  printf("Sum = ");
  matd_print_transpose(to_print->sum, "%f, ");
  printf("\n");
  printf("Squared sum =\n");
  matd_print(to_print->squared_sum, "%f ");
  printf("\n");
}

void point_accumulator_clear(point_accumulator_t *pa) {  
  for(int r = 0; r < pa->squared_sum->nrows; ++r) {
    matd_put(pa->sum, r, 0, 0.0);
    for(int c = 0; c < pa->squared_sum->ncols; ++c) {
      matd_put(pa->squared_sum, r, c, 0.0);
    }
  }
}

void point_accumulator_add_inplace(point_accumulator_t *pa_left, point_accumulator_t *pa_right) {
  assert(pa_left != NULL);
  assert(pa_right != NULL);
  
  matd_add_inplace(pa_left->sum, pa_right->sum);
  matd_add_inplace(pa_left->squared_sum, pa_right->squared_sum);
}

void point_accumulator_sub_inplace(point_accumulator_t *pa_left, 
				   point_accumulator_t *pa_right) {
  assert(pa_left != NULL);
  assert(pa_right != NULL);
  
  matd_subtract_inplace(pa_left->sum, pa_right->sum);
  matd_subtract_inplace(pa_left->squared_sum, pa_right->squared_sum);
}

void point_accumulator_add_point_inplace(point_accumulator_t *pa, matd_t *point) {
  matd_add_inplace(pa->sum, point);
  matd_t *point_transposed = matd_transpose(point);
  matd_t *squared_point = matd_multiply(point, point_transposed);
  matd_add_inplace(pa->squared_sum, squared_point);
  matd_destroy(point_transposed);
  matd_destroy(squared_point);
}

matd_t* point_accumulator_mean(point_accumulator_t *pa) {
  double n = pa->sum->data[pa->sum->nrows - 1];
  if(n > 0) {    
    return matd_scale(pa->sum, 1.0 / n);
  }
  return matd_create(pa->sum->nrows, 1);
}

matd_t* point_accumulator_covariance(point_accumulator_t *pa) {
  double n = pa->sum->data[pa->sum->nrows - 1];
  if(n > 0) {
    matd_t *mean = point_accumulator_mean(pa);
    matd_t *mean_transposed = matd_transpose(mean);
    matd_t *squared_mean = matd_multiply(mean, mean_transposed);
    matd_t *raw_covariance = matd_scale(pa->squared_sum, 1.0 / n);
    matd_subtract_inplace(raw_covariance, squared_mean);
    matd_t *covariance = matd_select(raw_covariance, 0, 2, 0, 2);
    matd_destroy(mean);
    matd_destroy(mean_transposed);
    matd_destroy(squared_mean);
    matd_destroy(raw_covariance);
    return covariance;
  }
  return matd_create(pa->squared_sum->nrows, pa->squared_sum->ncols);
}
