#include <unistd.h>
#include <inttypes.h>
#include <math.h>
#include "vx/vx.h"
#include "vx/webvx.h"
#include "common/time_util.h"
#include "common/http_advertiser.h"
#include "common/getopt.h"
#include "common/doubles.h"
#include "lcm/lcm.h"
#include "lcmtypes/diff_drive_t.h"
#include "lcmtypes/alarm_t.h"

#define WATCHDOG_PERIOD_MS    50 // ms
#define WATCHDOG_TIMEOUT_MS  700 // ms
#define DRIVE_PERIOD_MS       40 // ms
#define JOYPAD_BOUNDS_PX     200 // css pixels
#define JOYSTICK_SIZE_PX     100 // css pixels

#define MODE_JOYPAD 0
#define MODE_AUTON 1
#define DISABLE_BOX_X 750
#define DISABLE_BOX_Y 300

uint32_t next_id = 0;

typedef struct client client_t;
typedef struct state state_t;

#define MODE_BAR_SCALE   .1

struct client
{
    state_t *state;

    uint32_t id;

    vx_world_t *vw;
    vx_canvas_t *vc;
    vx_layer_t  *vl;
    pthread_t watchdog;
    pthread_t render_thread;

    // Joypad screen coordinates
    uint8_t finger_down;
    int finger_id;
    float current_x;
    float current_y;
    float initial_x;
    float initial_y;

    // Override interface
    uint8_t override;
    int second_finger_id;

    // Signal for threads
    uint8_t close;

    double last_echo;
    int echo_pending;

    double spinner_rad;
};

struct state
{
    lcm_t *lcm;

    webvx_t *webvx;
    zhash_t *clients;

    client_t *control_client;

    // Signal to stop publishing
    uint8_t mode;

    pthread_mutex_t mutex;

    getopt_t *gopt;
};

// mutex must be held.
void client_redraw_mode(client_t *client)
{
    // rotate the connection indicator
    vx_buffer_t *vb = vx_world_get_buffer(client->vw, "mode");
    vx_buffer_set_draw_order(vb, 1);
    vx_buffer_add_back(vb,
                       vxo_pixcoords(VXO_PIXCOORDS_TOP_LEFT,
                                     VXO_PIXCOORDS_SCALE_MODE_WIDTH,
                                     vxo_matrix_scale3(2, MODE_BAR_SCALE, 1),
                                     vxo_matrix_translate(0.5, -.5, 0),
                                     vxo_box_solid((float[]) { .3, .3, 1, 1 }),
                                     NULL),
                       NULL);
    vx_buffer_swap(vb);
}

int canvas_event_handler(vx_canvas_t *vc, const vx_event_t *ev, void *user)
{
    client_t *client = user;
    state_t *state = client->state;

    pthread_mutex_lock(&state->mutex);

    switch (ev->type) {
        case VX_EVENT_CANVAS_ECHO: {

            // Pet watchdog
            client->last_echo = ev->u.echo.nonce;
            client->echo_pending = 0;

            // rotate the connection indicator
            vx_buffer_t *vb = vx_world_get_buffer(client->vw, "spinner");
            vx_buffer_set_draw_order(vb, 2);
            vx_buffer_add_back(vb, vxo_depth_test(0,
                                                  vxo_pixcoords(
                                                      VXO_PIXCOORDS_TOP_RIGHT,
                                                      VXO_PIXCOORDS_SCALE_MODE_ONE,
                                                      vxo_chain(vxo_matrix_scale(40),
                                                                vxo_matrix_translate(-1.5, -1.5, 0),
                                                                vxo_matrix_rotatez(client->spinner_rad),
                                                                vxo_robot_solid((float[]) { 0, 1, 0, 1 }),
                                                                NULL),
                                                      NULL),
                                                  NULL),
                               NULL);

            vx_buffer_swap(vb);

            client->spinner_rad -= 0.1;
            if (client->spinner_rad < 0)
                client->spinner_rad += 2 * M_PI;
            break;
        }

        case VX_EVENT_CANVAS_CHANGE: {
            // Orientation change wedges touch interface. Start fresh.
            client->finger_down = 0;
            client->finger_id = -1;
            break;
        }
    }

    pthread_mutex_unlock(&state->mutex);

    return 0;
}

int layer_event_handler(vx_layer_t *vl, const vx_event_t *ev, void *user)
{
    client_t *client = user;
    state_t *state = client->state;

    pthread_mutex_lock(&state->mutex);

    if (ev->flags & VX_EVENT_FLAGS_SHIFT)
        client->override = 1;

    switch (ev->type) {
        case VX_EVENT_MOUSE_DOWN:
            printf("%f %f\n",ev->u.mouse_button.y, ev->viewport[3]);
            if (ev->u.mouse_button.y < MODE_BAR_SCALE * ev->viewport[2]) {
                printf("mode bar click\n");
            }
            break;
    }

    switch (ev->type) {

        case VX_EVENT_MOUSE_DOWN:
        case VX_EVENT_TOUCH_START: {

            // second finger initiates override
            if (client->finger_down && !client->override) {
                client->override = 1;
                client->second_finger_id = ev->u.touch.id;
            }

            // first finger down
            if (!client->finger_down) {
                // Screen rotation sometimes breaks this assertion
                //assert(client->finger_down == 0);

                client->finger_id = ev->u.touch.id;
                client->initial_x = ev->u.touch.x;;
                client->initial_y = ev->viewport[3] - ev->u.touch.y;
                client->current_x = client->initial_x;
                client->current_y = client->initial_y;

                client->finger_down = 1;

                // Grab robot control if nobody else has it.
                if (state->control_client == NULL) {
                    state->control_client = client;
                }

            }

            // if tap outside the drive area, toggle state between
            // autonomous and joypad.
            if(ev->u.touch.x > ev->viewport[2] - DISABLE_BOX_X
               && ev->u.touch.x < ev->viewport[2] - 0
               && ev->u.touch.y > ev->viewport[3] - DISABLE_BOX_Y
               && ev->u.touch.y < ev->viewport[3] - 0) {

                state->mode = (state->mode + 1) % 2;
            } else {
                state->mode = MODE_JOYPAD;
            }

            break;
        }

        case VX_EVENT_MOUSE_MOVED:
        case VX_EVENT_TOUCH_MOVE: {

            if (ev->u.touch.id == client->finger_id) {
                client->current_x = ev->u.touch.x;
                client->current_y = ev->viewport[3] - ev->u.touch.y;

            }
            break;
        }

        case VX_EVENT_MOUSE_UP:
        case VX_EVENT_TOUCH_END: {

            if (ev->u.touch.id == client->finger_id) {
                //assert(client->finger_down == 1);
                client->finger_down = 0;

            }

            // Release robot control if I have it.
            if (state->control_client == client) {
                state->control_client = NULL;
            }

            client->override = 0;

            break;
        }

        default: {
            //printf("unknown event %d\n", ev->type);

            break;
        }
    }

    // update display
    if (1) {
        vx_buffer_t *vb = vx_world_get_buffer(client->vw, "joypad");

        if (client->finger_down) {
            vx_buffer_add_back(vb, vxo_pixcoords(
                                   VXO_PIXCOORDS_BOTTOM_LEFT,
                                   VXO_PIXCOORDS_SCALE_MODE_ONE,
                                   vxo_chain(vxo_matrix_translate(client->initial_x, client->initial_y, 0),
                                             vxo_matrix_scale(JOYPAD_BOUNDS_PX),
                                             vxo_circle_line((float[]) { 1, 1, 1, 1 }, 2.0),
                                             NULL),
                                   NULL),
                               NULL);

            float knob_color[4] = { 1, 1, 0, 1 };
            const char *knob_message = NULL;
            if (client->override) {
                memcpy(knob_color, (float[]) { 1, 0, 0, 1 }, 4 * sizeof(float));
                knob_message = "safety off";
            }

            if (client != state->control_client) {
                memcpy(knob_color, (float[]) { .5, .5, .5, 1 }, 4 * sizeof(float));
                knob_message = "another joypad";
                printf("%p %p\n", client, state->control_client);
            }

            vx_buffer_add_back(vb, vxo_pixcoords(
                                   VXO_PIXCOORDS_BOTTOM_LEFT,
                                   VXO_PIXCOORDS_SCALE_MODE_ONE,
                                   vxo_chain(vxo_matrix_translate(client->current_x, client->current_y, 0),
                                             vxo_matrix_scale(JOYSTICK_SIZE_PX),
                                             vxo_circle_solid(knob_color),
                                             NULL),
                                   NULL),
                               NULL);

            if (knob_message) {
                vx_buffer_add_back(vb, vxo_pixcoords(
                                       VXO_PIXCOORDS_BOTTOM_LEFT,
                                       VXO_PIXCOORDS_SCALE_MODE_ONE,
                                       vxo_chain(//vxo_matrix_scale(4),
                                           vxo_matrix_translate(client->current_x, client->current_y + 1.5*JOYSTICK_SIZE_PX, 0),
                                           vxo_text(VXO_TEXT_ANCHOR_CENTER, "<<SansSerif-40,#ff0000>>%s", knob_message),
                                           NULL),
                                       NULL),
                                   NULL);
            }
        }

        vx_buffer_swap(vb);
    }

    pthread_mutex_unlock(&state->mutex);
    return 0;
}

void *watchdog_thread(void *user) {

    client_t *client = user;
    state_t *state = client->state;

    printf("starting watchdog for client %u\n", client->id);

    while (1) {
        usleep(WATCHDOG_PERIOD_MS * 1000);

        pthread_mutex_lock(&state->mutex);

        if (client->close) {
            printf("Client %u watchdog thread exiting\n", client->id);
            pthread_mutex_unlock(&state->mutex);
            return NULL;
        }

        if (!client->echo_pending) {
            double nonce = utime_now() / 1.0E6;
            vx_canvas_echo(client->vc, nonce);
            client->echo_pending = 1;
        }

        pthread_mutex_unlock(&state->mutex);
    }

    return NULL;
}

/*
void *render_thread(void *user) {

    client_t *client = user;
    state_t *state = client->state;

    for (int iter = 0; 1; iter++) {
        if (1) {

            vb = vx_world_get_buffer(client->vw, "nrobots");
            vx_buffer_add_back(vb, vxo_pixcoords(
                                   VXO_PIXCOORDS_BOTTOM_LEFT,
                                   VXO_PIXCOORDS_SCALE_MODE_ONE,
                                   vxo_chain(vxo_matrix_scale(4),
                                             vxo_text(VXO_TEXT_ANCHOR_BOTTOM_LEFT,
                                                      "%d",
                                                      nclients),
                                             NULL),
                                   NULL),
                               NULL);
            vx_buffer_swap(vb);

            vb = vx_world_get_buffer(client->vw, "disable");
            if(mode == MODE_AUTON)
            {
                vx_buffer_add_back(vb, vxo_pixcoords(
                                       VXO_PIXCOORDS_BOTTOM_RIGHT,
                                       VXO_PIXCOORDS_SCALE_MODE_ONE,
                                       vxo_chain(vxo_matrix_scale3(DISABLE_BOX_X, DISABLE_BOX_Y, 1),
                                                 vxo_square_solid((float[]){0,1,0,1}),
                                                 NULL),
                                       vxo_chain(vxo_matrix_scale(6),
                                                 vxo_matrix_translate(-5,0,0.1),
                                                 vxo_text(VXO_TEXT_ANCHOR_BOTTOM_RIGHT,
                                                          "AUTON"),
                                                 NULL),
                                       NULL),
                                   NULL);
            }
            else
            {
                vx_buffer_add_back(vb, vxo_pixcoords(
                                       VXO_PIXCOORDS_BOTTOM_RIGHT,
                                       VXO_PIXCOORDS_SCALE_MODE_ONE,
                                       vxo_chain(vxo_matrix_scale3(DISABLE_BOX_X, DISABLE_BOX_Y, 1),
                                                 vxo_square_solid((float[]){0,0,1,1}),
                                                 NULL),
                                       vxo_chain(vxo_matrix_scale(6),
                                                 vxo_matrix_translate(-5,0,0.1),
                                                 vxo_text(VXO_TEXT_ANCHOR_BOTTOM_RIGHT,
                                                          "JOYPD"),
                                                 NULL),
                                       NULL),
                                   NULL);

            }
            vx_buffer_swap(vb);
        }

        usleep(100*1000);
    }
}
*/
void *drive_thread(void *user) {

    state_t *state = user;

    while (1) {

        usleep(DRIVE_PERIOD_MS * 1000);

        pthread_mutex_lock(&state->mutex);

        diff_drive_t msg = {
            .utime = utime_now(),
            .left = 0.0,
            .left_enabled = 1,
            .right = 0.0,
            .right_enabled = 1,
        };

        client_t *client = state->control_client;

        if (client) {
            double watchdog_dt = utime_now() / 1.0E6 - client->last_echo;
            double xy[2] = { client->current_x - client->initial_x,
                             client->current_y - client->initial_y };

            if (watchdog_dt > WATCHDOG_TIMEOUT_MS / 1.0E3) {

                fprintf(stderr, "Watchdog tripped on %u, RTT = %f s\n", client->id, watchdog_dt);
                // Send Alarm
                char message[256];
                sprintf(message, "Joypad Watchdog %f ms\n", watchdog_dt * 1E3);
                alarm_t alarm = {
                    .utime = utime_now(),
                    .topic = "Joypad Drive Watchdog",
                    .message = message
                };
                alarm_t_publish(state->lcm, "ALARM_JOYPAD", &alarm);

                // un-elect this client; let someone else take over control.
//                state->control_client = NULL;

            } else {

                // watchdog NOT fired.

                if (client->finger_down) {

                    if (doubles_magnitude(xy, 2) > JOYPAD_BOUNDS_PX) {
                        doubles_normalize_self(xy, 2);
                        doubles_scale_self(xy, 2, JOYPAD_BOUNDS_PX);
                    }

                    // send drive commands
                    double speed = xy[1] / JOYPAD_BOUNDS_PX;
                    double turn = xy[0] / JOYPAD_BOUNDS_PX;

                    double LR[] = {speed + turn,
                                   speed - turn};

                    // normalize
                    double max = fmax(fabs(LR[0]), fabs(LR[1]));
                    if (max > 1.0) {
                        LR[0] /= max;
                        LR[1] /= max;
                    }

                    msg.left = LR[0];
                    msg.right = LR[1];

                    printf("%15f %15f\n", msg.left, msg.right);

                    diff_drive_t_publish(state->lcm,
                                         client->override ?
                                           getopt_get_string(state->gopt, "channel-override") :
                                           getopt_get_string(state->gopt, "channel"),
                                         &msg);
                }
            }
        }

        pthread_mutex_unlock(&state->mutex);
    }
}

void on_create_canvas(vx_canvas_t *vc, const char *name, void *impl)
{
    //if (next_id == 1) return;
    printf("canvas at %" PRIu64 "\n", (uint64_t)vc);

    state_t *state = impl;

    printf("ON CREATE CANVAS\n");
    client_t *client = calloc(1, sizeof(client_t));

    // set up client environment
    client->id = next_id++;

    client->last_echo = 0;

    client->finger_down = 0;
    client->finger_id = -1;

    client->state = state;
    client->vw = vx_world_create();
    client->vc = vc;
    char layer_name[32];
    sprintf(layer_name, "default_%u", client->id);
    client->vl = vx_canvas_get_layer(vc, layer_name);
    vx_layer_set_world(client->vl, client->vw);
    vx_canvas_set_title(client->vc, "Joypad Drive");
    vx_layer_add_event_handler(client->vl, layer_event_handler, 0, client);
    vx_canvas_add_event_handler(client->vc, canvas_event_handler, 0, client);
//    vx_layer_enable_touch_camera_controls(client->vl, 0); // turn off touch camera controls

    // start threads
    client->close = 0;
    pthread_create(&client->watchdog, NULL, watchdog_thread, client);

    // Add client to zhash
    zhash_put(state->clients, &client->vc, &client, NULL, NULL);
    printf("clients: %u \n", zhash_size(state->clients));

    client_redraw_mode(client);

    client_t *client_test = NULL;
    zhash_get(state->clients, &client->vc, &client_test);
    assert(client_test == client);
}

void on_destroy_canvas(vx_canvas_t *vc, void *impl)
{
    state_t *state = impl;

    printf("ON DESTROY CANVAS\n");
    // XXX: Clean up
    client_t *client = NULL;
    zhash_get(state->clients, &vc, &client);

    // Release robot control if I have it.
    pthread_mutex_lock(&state->mutex);
    if(state->control_client == client) {
        fprintf(stderr, "CONTROL CLIENT DIED???\n");
        state->control_client = NULL;
    }
    pthread_mutex_unlock(&state->mutex);

    client->close = 1;
    pthread_join(client->watchdog, NULL);
    pthread_join(client->render_thread, NULL);

    free(client);

    zhash_remove(state->clients, &vc, NULL, NULL);
}

int main(int argc, char *argv[])
{
    setlinebuf(stderr);
    setlinebuf(stdout);

    state_t *state = calloc(1, sizeof(state_t));
    state->lcm = lcm_create(NULL);
    pthread_mutex_init(&state->mutex, NULL);
    state->clients = zhash_create(sizeof(vx_canvas_t*), sizeof(client_t*),
                                  zhash_ptr_hash, zhash_ptr_equals);
    state->control_client = NULL;

    getopt_t * gopt = getopt_create();
    state->gopt = gopt;

    getopt_add_bool(gopt, 'h', "help", 0, "Show this help");
    getopt_add_string(gopt, 'c', "channel", "DIFF_DRIVE_HUMAN", "What LCM Channel to publish on");
    getopt_add_string(gopt, 'C', "channel-override", "DIFF_DRIVE_HUMAN_OVERRIDE", "What LCM Channel to publish on");
    getopt_add_int(gopt, 'p', "port", "8123", "webvx port");

    if (!getopt_parse(gopt, argc, argv, 1) || getopt_get_bool(gopt, "help")) {
        printf("Usage: %s [options] <input files>\n", argv[0]);
        getopt_do_usage(gopt);
        exit(0);
    }
    int port = getopt_get_int(gopt,"port");

    state->webvx = webvx_create_server(port, NULL, "index.html");
    http_advertiser_t * advertiser = http_advertiser_create(state->lcm, port, "JoyPad Drive", "Manual Control");

    webvx_define_canvas(state->webvx, "mycanvas", on_create_canvas, on_destroy_canvas, state);

    pthread_t drive;
    pthread_create(&drive, NULL, drive_thread, state);

    while (1) {
        usleep(10000);
    }
}
