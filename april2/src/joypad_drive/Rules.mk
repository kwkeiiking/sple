.PHONY: joypad_drive joypad_drive_clean

joypad_drive: april_lcmtypes vx

joypad_drive:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/joypad_drive -f Build.mk

joypad_drive_clean:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/joypad_drive -f Build.mk clean

all: joypad_drive

clean: joypad_drive_clean
