SRCS = $(shell ls *.c)
OBJS = $(SRCS:%.c=%.o)

CFLAGS := $(CFLAGS_STD) -O3

include $(BUILD_COMMON)

all: $(LIB_PATH)/libutest.a
	@/bin/true

$(LIB_PATH)/libutest.a: $(OBJS)
	@$(AR) rc $@ $^

.PHONY: clean

clean:
	@rm -rf *.o $(LIB_PATH)/libutest.a
