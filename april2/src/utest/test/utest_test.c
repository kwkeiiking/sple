/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

/**
 * UTEST Tests
 * Sanity checks for the testing library
 */

#include <stdio.h>
#include <stdint.h>
#include "utest/utest.h"


void test_group_int()
{
    int a = 1;
    TEST_ASSERT_EQUAL_INT(a, a);
    int8_t b = 1<<7;
    TEST_ASSERT_EQUAL_INT8(b, b);
    int16_t c = 1<<15;
    TEST_ASSERT_EQUAL_INT16(c, c);
    int32_t d = 1<<31;
    TEST_ASSERT_EQUAL_INT32(d, d);
    int64_t e = 1<<31;
    TEST_ASSERT_EQUAL_INT64(e, e);
    uint8_t f = 1;
    TEST_ASSERT_EQUAL_UINT(f, f);
    uint8_t g = 1<<7;
    TEST_ASSERT_EQUAL_UINT8(g, g);
    int16_t h = 1<<15;
    TEST_ASSERT_EQUAL_UINT16(h, h);
    int32_t i = 1<<31;
    TEST_ASSERT_EQUAL_UINT32(i, i);
    int64_t j = 1<<31;
    TEST_ASSERT_EQUAL_UINT64(j, j);
}

void test_group_bits()
{
    TEST_ASSERT_BITS(0b11110000, 0b10101010, 0b10101111);
    TEST_ASSERT_BITS_HIGH(0b11110000, 0b11110000);
    TEST_ASSERT_BITS_LOW(0b11110000, 0b00001111);
    TEST_ASSERT_BIT_HIGH(2, 0b11110100);
    TEST_ASSERT_BIT_LOW(2, 0b00001011);
}

void test_group_int_ranges()
{
    int a = 1;
    TEST_ASSERT_INT_WITHIN(1, a, a);
    int8_t b = 1<<7;
    TEST_ASSERT_INT8_WITHIN(1, b, b);
    int16_t c = 1<<15;
    TEST_ASSERT_INT16_WITHIN(1, c, c);
    int32_t d = 1<<31;
    TEST_ASSERT_INT32_WITHIN(1, d, d);
    int64_t e = 1<<31;
    TEST_ASSERT_INT64_WITHIN(1, e, e);
    uint8_t f = 1;
    TEST_ASSERT_UINT_WITHIN(1, f, f);
    uint8_t g = 1<<7;
    TEST_ASSERT_UINT8_WITHIN(1, g, g);
    int16_t h = 1<<15;
    TEST_ASSERT_UINT16_WITHIN(1, h, h);
    int32_t i = 1<<31;
    TEST_ASSERT_UINT32_WITHIN(1, i, i);
    int64_t j = 1<<31;
    TEST_ASSERT_UINT64_WITHIN(1, j, j);
}

void test_group_int_array()
{
    int a[] = {1};
    TEST_ASSERT_EQUAL_INT_ARRAY(a, a, 1);
    int8_t b[] = {1<<7};
    TEST_ASSERT_EQUAL_INT8_ARRAY(b, b, 1);
    int16_t c[] = {1<<15};
    TEST_ASSERT_EQUAL_INT16_ARRAY(c, c, 1);
    int32_t d[] = {1<<31};
    TEST_ASSERT_EQUAL_INT32_ARRAY(d, d, 1);
    int64_t e[] = {1<<31};
    TEST_ASSERT_EQUAL_INT64_ARRAY(e, e, 1);
    uint8_t f[] = {1};
    TEST_ASSERT_EQUAL_UINT_ARRAY(f, f, 1);
    uint8_t g[] = {1<<7};
    TEST_ASSERT_EQUAL_UINT8_ARRAY(g, g, 1);
    int16_t h[] = {1<<15};
    TEST_ASSERT_EQUAL_UINT16_ARRAY(h, h, 1);
    int32_t i[] = {1<<31};
    TEST_ASSERT_EQUAL_UINT32_ARRAY(i, i, 1);
    int64_t j[] = {1<<31};
    TEST_ASSERT_EQUAL_UINT64_ARRAY(j, j, 1);
}

void test_group_float()
{
    float a = 1.0f;
    float del = 1e-6f;
    TEST_ASSERT_FLOAT_WITHIN(del, a, a);
    TEST_ASSERT_EQUAL_FLOAT(del, a, a);
    float arr[] = {1e-5f};
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(del, arr, arr, 1);
}

void test_group_double()
{
    float a = 1.0;
    float del = 1e-12;
    TEST_ASSERT_DOUBLE_WITHIN(del, a, a);
    TEST_ASSERT_EQUAL_DOUBLE(del, a, a);
    float arr[] = {1e-24f};
    TEST_ASSERT_EQUAL_DOUBLE_ARRAY(del, arr, arr, 1);
}

void test_group_memory()
{
    void *ptr1, *ptr2;
    int a;
    ptr1 = &a;
    ptr2 = &a;
    TEST_ASSERT_EQUAL_PTR(ptr1, ptr2);
    TEST_ASSERT_EQUAL_MEMORY(ptr1, ptr2, sizeof(void *));
}

void test_group_string()
{
    TEST_ASSERT_EQUAL_STRING("APRIL", "APRIL");
    TEST_ASSERT_EQUAL_STRING_TILL_LEN("APRIL", "APRIL BBB3912", 5);
}

void test_group_bool()
{
    TEST_ASSERT(true);
    TEST_ASSERT(!false);
    TEST_ASSERT_UNLESS(false);
    TEST_ASSERT_FALSE(false);
    TEST_ASSERT_NULL(NULL);
    TEST_ASSERT_NOT_NULL("");
    TEST_ASSERT_NOT_EQUAL(1,2);

}

int main()
{
    test_group_int();
    test_group_bits();
    test_group_int_ranges();
    test_group_int_array();
    test_group_float();
    test_group_double();
    test_group_memory();
    test_group_string();
    test_group_bool();
    return 0;
}
