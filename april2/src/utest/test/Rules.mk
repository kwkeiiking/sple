.PHONY: utest_test_clean

utest_test: utest

utest_test:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/utest/test/ -f Build.mk test

utest_test_clean:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/utest/test -f Build.mk clean

all:

test: utest_test

clean: utest_test_clean
