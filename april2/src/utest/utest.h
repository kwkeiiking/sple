/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Unit Testing Library
 *
 * See the API section at the end of the file
 *
 * MIT License ? - unity
 */

#ifndef APRIL_UTEST
#define APRIL_UTEST

#include <stdio.h>
#include <stdbool.h>
#include <math.h>


/* Determine the size of an int, if not already specificied.
 * Infer it from UINT_MAX if possible. */
#ifndef UTEST_INT_WIDTH
  #ifdef UINT_MAX
    #if (UINT_MAX == 0xFFFF)
      #define UTEST_INT_WIDTH (16)
    #elif (UINT_MAX == 0xFFFFFFFF)
      #define UTEST_INT_WIDTH (32)
    #elif (UINT_MAX == 0xFFFFFFFFFFFFFFFF)
      #define UTEST_INT_WIDTH (64)
    #endif
  #endif
#endif
#ifndef UTEST_INT_WIDTH
  #define UTEST_INT_WIDTH (32)
#endif

/* Determine the size of a long, if not already specified,
 * by following the process used above to define UTEST_INT_WIDTH. */
#ifndef UTEST_LONG_WIDTH
  #ifdef ULONG_MAX
    #if (ULONG_MAX == 0xFFFF)
      #define UTEST_LONG_WIDTH (16)
    #elif (ULONG_MAX == 0xFFFFFFFF)
      #define UTEST_LONG_WIDTH (32)
    #elif (ULONG_MAX == 0xFFFFFFFFFFFFFFFF)
      #define UTEST_LONG_WIDTH (64)
    #endif
  #endif
#endif
#ifndef UTEST_LONG_WIDTH
  #define UTEST_LONG_WIDTH (32)
#endif

/* Determine the size of a pointer, if not already specified,
 * by following the process used above to define UTEST_INT_WIDTH. */
#ifndef UTEST_POINTER_WIDTH
  #ifdef UINTPTR_MAX
    #if (UINTPTR_MAX+0 <= 0xFFFF)
      #define UTEST_POINTER_WIDTH (16)
    #elif (UINTPTR_MAX+0 <= 0xFFFFFFFF)
      #define UTEST_POINTER_WIDTH (32)
    #elif (UINTPTR_MAX+0 <= 0xFFFFFFFFFFFFFFFF)
      #define UTEST_POINTER_WIDTH (64)
    #endif
  #endif
#endif
#ifndef UTEST_POINTER_WIDTH
  #ifdef INTPTR_MAX
    #if (INTPTR_MAX+0 <= 0x7FFF)
      #define UTEST_POINTER_WIDTH (16)
    #elif (INTPTR_MAX+0 <= 0x7FFFFFFF)
      #define UTEST_POINTER_WIDTH (32)
    #elif (INTPTR_MAX+0 <= 0x7FFFFFFFFFFFFFFF)
      #define UTEST_POINTER_WIDTH (64)
    #endif
  #endif
#endif
#ifndef UTEST_POINTER_WIDTH
  #define UTEST_POINTER_WIDTH UTEST_LONG_WIDTH
#endif


#if (UTEST_INT_WIDTH == 32)
    typedef unsigned char   uu8_t;
    typedef unsigned short  uu16_t;
    typedef unsigned int    uu32_t;
    typedef signed char     us8_t;
    typedef signed short    us16_t;
    typedef signed int      us32_t;
#elif (UTEST_INT_WIDTH == 16)
    typedef unsigned char   uu8_t;
    typedef unsigned int    uu16_t;
    typedef unsigned long   uu32_t;
    typedef signed char     us8_t;
    typedef signed int      us16_t;
    typedef signed long     us32_t;
#else
    #error Invalid UTEST_INT_WIDTH specified! (16 or 32 are supported)
#endif

#ifndef UTEST_SUPPORT_64
#if UTEST_LONG_WIDTH > 32
#define UTEST_SUPPORT_64
#endif
#endif
#ifndef UTEST_SUPPORT_64
#if UTEST_POINTER_WIDTH > 32
#define UTEST_SUPPORT_64
#endif
#endif

#ifndef UTEST_SUPPORT_64
/* No 64-bit Support */
typedef uu32_t u_uint_t;
typedef us32_t u_sint_t;
#else
/* 64-bit Support */
#if (UTEST_LONG_WIDTH == 32)
    typedef unsigned long long uu64_t;
    typedef signed long long   us64_t;
#elif (UTEST_LONG_WIDTH == 64)
    typedef unsigned long      uu64_t;
    typedef signed long        us64_t;
#else
    #error Invalid UTEST_LONG_WIDTH specified! (32 or 64 are supported)
#endif
typedef uu64_t u_uint_t;
typedef us64_t u_sint_t;

#endif

/* Used for printing only */
static const u_uint_t UTEST_SIZE_MASK[] =
{
    255u,         /* 0xFF */
    65535u,       /* 0xFFFF */
    65535u,
    4294967295u,  /* 0xFFFFFFFF */
    4294967295u,
    4294967295u,
    4294967295u
#ifdef UTEST_SUPPORT_64
    ,0xFFFFFFFFFFFFFFFF
#endif
};


#define UTEST_DISPLAY_RANGE_INT  (0x10)
#define UTEST_DISPLAY_RANGE_UINT (0x20)
#define UTEST_DISPLAY_RANGE_HEX  (0x40)
#define UTEST_DISPLAY_RANGE_AUTO (0x80)

typedef enum
{
#if (UTEST_INT_WIDTH == 16)
    UTEST_DISPLAY_STYLE_INT      = 2 + UTEST_DISPLAY_RANGE_INT + UTEST_DISPLAY_RANGE_AUTO,
#elif (UTEST_INT_WIDTH  == 32)
    UTEST_DISPLAY_STYLE_INT      = 4 + UTEST_DISPLAY_RANGE_INT + UTEST_DISPLAY_RANGE_AUTO,
#elif (UTEST_INT_WIDTH  == 64)
    UTEST_DISPLAY_STYLE_INT      = 8 + UTEST_DISPLAY_RANGE_INT + UTEST_DISPLAY_RANGE_AUTO,
#endif
    UTEST_DISPLAY_STYLE_INT8     = 1 + UTEST_DISPLAY_RANGE_INT,
    UTEST_DISPLAY_STYLE_INT16    = 2 + UTEST_DISPLAY_RANGE_INT,
    UTEST_DISPLAY_STYLE_INT32    = 4 + UTEST_DISPLAY_RANGE_INT,
#ifdef UTEST_SUPPORT_64
    UTEST_DISPLAY_STYLE_INT64    = 8 + UTEST_DISPLAY_RANGE_INT,
#endif

#if (UTEST_INT_WIDTH == 16)
    UTEST_DISPLAY_STYLE_UINT     = 2 + UTEST_DISPLAY_RANGE_UINT + UTEST_DISPLAY_RANGE_AUTO,
#elif (UTEST_INT_WIDTH  == 32)
    UTEST_DISPLAY_STYLE_UINT     = 4 + UTEST_DISPLAY_RANGE_UINT + UTEST_DISPLAY_RANGE_AUTO,
#elif (UTEST_INT_WIDTH  == 64)
    UTEST_DISPLAY_STYLE_UINT     = 8 + UTEST_DISPLAY_RANGE_UINT + UTEST_DISPLAY_RANGE_AUTO,
#endif
    UTEST_DISPLAY_STYLE_UINT8    = 1 + UTEST_DISPLAY_RANGE_UINT,
    UTEST_DISPLAY_STYLE_UINT16   = 2 + UTEST_DISPLAY_RANGE_UINT,
    UTEST_DISPLAY_STYLE_UINT32   = 4 + UTEST_DISPLAY_RANGE_UINT,
#ifdef UTEST_SUPPORT_64
    UTEST_DISPLAY_STYLE_UINT64   = 8 + UTEST_DISPLAY_RANGE_UINT,
#endif
//     UTEST_DISPLAY_STYLE_HEX8     = 1 + UTEST_DISPLAY_RANGE_HEX,
//     UTEST_DISPLAY_STYLE_HEX16    = 2 + UTEST_DISPLAY_RANGE_HEX,
//     UTEST_DISPLAY_STYLE_HEX32    = 4 + UTEST_DISPLAY_RANGE_HEX,
// #ifdef UTEST_SUPPORT_64
//     UTEST_DISPLAY_STYLE_HEX64    = 8 + UTEST_DISPLAY_RANGE_HEX,
// #endif
    UTEST_DISPLAY_STYLE_UNKNOWN
} UTEST_DISPLAY_STYLE_T;

#ifndef UTEST_LINE_TYPE
#define UTEST_LINE_TYPE u_uint_t
#endif

#if (UTEST_POINTER_WIDTH == 32)
    typedef uu32_t u_ptr_t;
#define UTEST_DISPLAY_STYLE_POINTER UTEST_DISPLAY_STYLE_UINT32
#elif (UTEST_POINTER_WIDTH == 64)
    typedef uu64_t u_ptr_t;
#define UTEST_DISPLAY_STYLE_POINTER UTEST_DISPLAY_STYLE_UINT64
#elif (UTEST_POINTER_WIDTH == 16)
    typedef uu16_t u_ptr_t;
#define UTEST_DISPLAY_STYLE_POINTER UTEST_DISPLAY_STYLE_UINT16
#else
    #error Invalid UTEST_POINTER_WIDTH specified! (16, 32 or 64 are supported)
#endif

#ifndef UTEST_INTERNAL_PTR
#define UTEST_INTERNAL_PTR const void*
#endif

/* Floating Point Support */
#ifndef UTEST_FLOAT_TYPE
#define UTEST_FLOAT_TYPE float
#endif
typedef UTEST_FLOAT_TYPE u_float_t;

#ifndef isneg
#define isneg(n) ((n < 0.0f) ? 1 : 0)
#endif

#ifndef ispos
#define ispos(n) ((n > 0.0f) ? 1 : 0)
#endif


#ifndef UNITY_DOUBLE_TYPE
#define UNITY_DOUBLE_TYPE double
#endif
typedef UNITY_DOUBLE_TYPE u_double_t;

void utest_assert_condition(const bool condition,
                            const UTEST_LINE_TYPE line_number,
                            const char *file);
void utest_assert_equal_bits(const u_sint_t mask,
                             const u_sint_t expected,
                             const u_sint_t actual,
                             const UTEST_LINE_TYPE line_number,
                             const char *file);
void utest_assert_equal_number(const u_sint_t expected,
                               const u_sint_t actual,
                               const UTEST_LINE_TYPE line_number,
                               const char *file,
                               const UTEST_DISPLAY_STYLE_T style);
void utest_assert_number_within(const u_uint_t delta,
                                const u_sint_t expected,
                                const u_sint_t actual,
                                const UTEST_LINE_TYPE line_number,
                                const char *file,
                                const UTEST_DISPLAY_STYLE_T style);
void utest_assert_equal_int_array(UTEST_INTERNAL_PTR expected,
                                  UTEST_INTERNAL_PTR actual,
                                  const uu32_t num_elements,
                                  const UTEST_LINE_TYPE line_number,
                                  const char *file,
                                  const UTEST_DISPLAY_STYLE_T style);
void utest_assert_floats_within(const u_float_t delta,
                                const u_float_t expected,
                                const u_float_t actual,
                                const UTEST_LINE_TYPE line_number,
                                const char *file);
void utest_assert_equal_float_array(const u_float_t tolerance,
                                    const u_float_t* expected,
                                    const u_float_t* actual,
                                    const uu32_t num_elements,
                                    const UTEST_LINE_TYPE line_number,
                                    const char* file);
void utest_assert_doubles_within(const u_double_t delta,
                                 const u_double_t expected,
                                 const u_double_t actual,
                                 const UTEST_LINE_TYPE line_number,
                                 const char *file);
void utest_assert_equal_double_array(const u_double_t tolerance,
                                     const u_double_t* expected,
                                     const u_double_t* actual,
                                     const uu32_t num_elements,
                                     const UTEST_LINE_TYPE line_number,
                                     const char* file);
void utest_assert_equal_string(const char* expected,
                               const char* actual,
                               const UTEST_LINE_TYPE line_number,
                               const char* file);
void utest_assert_equal_string_till_len(const char* expected,
                                        const char* actual,
                                        const uu32_t length,
                                        const UTEST_LINE_TYPE line_number,
                                        const char* file);
void utest_assert_equal_memory(UTEST_INTERNAL_PTR expected,
                               UTEST_INTERNAL_PTR actual,
                               const uu32_t length,
                               const uu32_t num_elements,
                               const UTEST_LINE_TYPE line_number,
                               const char* file);
void utest_test_suite_begin(const char * file);


#define UTEST_TEST_ASSERT(condition, line, file)                                              utest_assert_condition(condition, (UTEST_LINE_TYPE)line, file)
#define UTEST_TEST_ASSERT_NULL(pointer, line, file)                                           UTEST_TEST_ASSERT(((pointer) == NULL), (UTEST_LINE_TYPE)(line), file)
#define UTEST_TEST_ASSERT_NOT_NULL(pointer, line, file)                                       UTEST_TEST_ASSERT(((pointer) != NULL), (UTEST_LINE_TYPE)(line), file)

#define UTEST_TEST_ASSERT_EQUAL_INT(expected, actual, line, file)                             utest_assert_equal_number((u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT)
#define UTEST_TEST_ASSERT_EQUAL_INT8(expected, actual, line, file)                            utest_assert_equal_number((u_sint_t)(us8_t )(expected), (u_sint_t)(us8_t )(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT8)
#define UTEST_TEST_ASSERT_EQUAL_INT16(expected, actual, line, file)                           utest_assert_equal_number((u_sint_t)(us16_t)(expected), (u_sint_t)(us16_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT16)
#define UTEST_TEST_ASSERT_EQUAL_INT32(expected, actual, line, file)                           utest_assert_equal_number((u_sint_t)(us32_t)(expected), (u_sint_t)(us32_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT32)
#define UTEST_TEST_ASSERT_EQUAL_UINT(expected, actual, line, file)                            utest_assert_equal_number((u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT)
#define UTEST_TEST_ASSERT_EQUAL_UINT8(expected, actual, line, file)                           utest_assert_equal_number((u_sint_t)(uu8_t )(expected), (u_sint_t)(uu8_t )(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT8)
#define UTEST_TEST_ASSERT_EQUAL_UINT16(expected, actual, line, file)                          utest_assert_equal_number((u_sint_t)(uu16_t)(expected), (u_sint_t)(uu16_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT16)
#define UTEST_TEST_ASSERT_EQUAL_UINT32(expected, actual, line, file)                          utest_assert_equal_number((u_sint_t)(uu32_t)(expected), (u_sint_t)(uu32_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT32)
#define UTEST_TEST_ASSERT_EQUAL_INT64(expected, actual, line, file)                           utest_assert_equal_number((u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT64)
#define UTEST_TEST_ASSERT_EQUAL_UINT64(expected, actual, line, file)                          utest_assert_equal_number((u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT64)
// #define UTEST_TEST_ASSERT_EQUAL_HEX8(expected, actual, line, file)                            utest_assert_equal_number((u_sint_t)(us8_t )(expected), (u_sint_t)(us8_t )(actual), (UTEST_LINE_TYPE)(line), UTEST_DISPLAY_STYLE_HEX8)
// #define UTEST_TEST_ASSERT_EQUAL_HEX16(expected, actual, line, file)                           utest_assert_equal_number((u_sint_t)(us16_t)(expected), (u_sint_t)(us16_t)(actual), (UTEST_LINE_TYPE)(line), UTEST_DISPLAY_STYLE_HEX16)
// #define UTEST_TEST_ASSERT_EQUAL_HEX32(expected, actual, line, file)                           utest_assert_equal_number((u_sint_t)(us32_t)(expected), (u_sint_t)(us32_t)(actual), (UTEST_LINE_TYPE)(line), UTEST_DISPLAY_STYLE_HEX32)
#define UTEST_TEST_ASSERT_BITS(mask, expected, actual, line, file)                            utest_assert_equal_bits((u_sint_t)(mask), (u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file)

#define UTEST_TEST_ASSERT_INT_WITHIN(delta, expected, actual, line, file)                     utest_assert_number_within((delta), (u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT)
#define UTEST_TEST_ASSERT_INT8_WITHIN(delta, expected, actual, line, file)                    utest_assert_number_within((uu8_t )(delta), (u_sint_t)(us8_t )(expected), (u_sint_t)(us8_t )(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT8)
#define UTEST_TEST_ASSERT_INT16_WITHIN(delta, expected, actual, line, file)                   utest_assert_number_within((uu16_t)(delta), (u_sint_t)(us16_t)(expected), (u_sint_t)(us16_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT16)
#define UTEST_TEST_ASSERT_INT32_WITHIN(delta, expected, actual, line, file)                   utest_assert_number_within((uu32_t)(delta), (u_sint_t)(us32_t)(expected), (u_sint_t)(us32_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT32)
#define UTEST_TEST_ASSERT_UINT_WITHIN(delta, expected, actual, line, file)                    utest_assert_number_within((delta), (u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT)
#define UTEST_TEST_ASSERT_UINT8_WITHIN(delta, expected, actual, line, file)                   utest_assert_number_within((uu8_t )(delta), (u_sint_t)(u_uint_t)(uu8_t )(expected), (u_sint_t)(u_uint_t)(uu8_t )(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT8)
#define UTEST_TEST_ASSERT_UINT16_WITHIN(delta, expected, actual, line, file)                  utest_assert_number_within((uu16_t)(delta), (u_sint_t)(u_uint_t)(uu16_t)(expected), (u_sint_t)(u_uint_t)(uu16_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT16)
#define UTEST_TEST_ASSERT_UINT32_WITHIN(delta, expected, actual, line, file)                  utest_assert_number_within((uu32_t)(delta), (u_sint_t)(u_uint_t)(uu32_t)(expected), (u_sint_t)(u_uint_t)(uu32_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT32)
#define UTEST_TEST_ASSERT_INT64_WITHIN(delta, expected, actual, line, file)                   utest_assert_number_within((delta), (u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT64)
#define UTEST_TEST_ASSERT_UINT64_WITHIN(delta, expected, actual, line, file)                  utest_assert_number_within((delta), (u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT64)
// #define UTEST_TEST_ASSERT_HEX8_WITHIN(delta, expected, actual, line)                    utest_assert_numbers_within((uu8_t )(delta), (u_sint_t)(u_uint_t)(uu8_t )(expected), (u_sint_t)(u_uint_t)(uu8_t )(actual), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX8)
// #define UTEST_TEST_ASSERT_HEX16_WITHIN(delta, expected, actual, line)                   utest_assert_numbers_within((uu16_t)(delta), (u_sint_t)(u_uint_t)(uu16_t)(expected), (u_sint_t)(u_uint_t)(uu16_t)(actual), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX16)
// #define UTEST_TEST_ASSERT_HEX32_WITHIN(delta, expected, actual, line)                   utest_assert_numbers_within((uu32_t)(delta), (u_sint_t)(u_uint_t)(uu32_t)(expected), (u_sint_t)(u_uint_t)(uu32_t)(actual), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX32)

#define UTEST_TEST_ASSERT_EQUAL_PTR(expected, actual, line, file)                             utest_assert_equal_number((u_sint_t)(u_ptr_t)(expected), (u_sint_t)(u_ptr_t)(actual), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_POINTER)
#define UTEST_TEST_ASSERT_EQUAL_STRING(expected, actual, line, file)                          utest_assert_equal_string((const char*)(expected), (const char*)(actual), (UTEST_LINE_TYPE)(line), file)
#define UTEST_TEST_ASSERT_EQUAL_STRING_TILL_LEN(expected, actual, len, line, file)            utest_assert_equal_string_till_len((const char*)(expected), (const char*)(actual), (uu32_t)(len), (UTEST_LINE_TYPE)(line), file)
#define UTEST_TEST_ASSERT_EQUAL_MEMORY(expected, actual, len, line, file)                     utest_assert_equal_memory((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(len), 1, (UTEST_LINE_TYPE)(line), file)
// #define UTEST_TEST_ASSERT_EQUAL_PTR_ARRAY(expected, actual, num_elements, line, file)         utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(u_ptr_t*)(expected), (UTEST_INTERNAL_PTR)(u_ptr_t*)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_POINTER)
// #define UTEST_TEST_ASSERT_EQUAL_STRING_ARRAY(expected, actual, num_elements, line, file)      utest_assert_equal_string_array((const char**)(expected), (const char**)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line))
// #define UTEST_TEST_ASSERT_EQUAL_MEMORY_ARRAY(expected, actual, len, num_elements, line, file) utest_assert_equal_memory((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(len), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line))

#define UTEST_TEST_ASSERT_EQUAL_INT_ARRAY(expected, actual, num_elements, line, file)         utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT)
#define UTEST_TEST_ASSERT_EQUAL_INT8_ARRAY(expected, actual, num_elements, line, file)        utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT8)
#define UTEST_TEST_ASSERT_EQUAL_INT16_ARRAY(expected, actual, num_elements, line, file)       utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT16)
#define UTEST_TEST_ASSERT_EQUAL_INT32_ARRAY(expected, actual, num_elements, line, file)       utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT32)
#define UTEST_TEST_ASSERT_EQUAL_UINT_ARRAY(expected, actual, num_elements, line, file)        utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT)
#define UTEST_TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, actual, num_elements, line, file)       utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT8)
#define UTEST_TEST_ASSERT_EQUAL_UINT16_ARRAY(expected, actual, num_elements, line, file)      utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT16)
#define UTEST_TEST_ASSERT_EQUAL_UINT32_ARRAY(expected, actual, num_elements, line, file)      utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT32)
#define UTEST_TEST_ASSERT_EQUAL_INT64_ARRAY(expected, actual, num_elements, line, file)       utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_INT64)
#define UTEST_TEST_ASSERT_EQUAL_UINT64_ARRAY(expected, actual, num_elements, line, file)      utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file, UTEST_DISPLAY_STYLE_UINT64)
// #define UTEST_TEST_ASSERT_EQUAL_HEX8_ARRAY(expected, actual, num_elements, line)        utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX8)
// #define UTEST_TEST_ASSERT_EQUAL_HEX16_ARRAY(expected, actual, num_elements, line)       utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX16)
// #define UTEST_TEST_ASSERT_EQUAL_HEX32_ARRAY(expected, actual, num_elements, line)       utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX32)

// #define UTEST_TEST_ASSERT_EQUAL_HEX64(expected, actual, line)                           utest_assert_equal_number((u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX64)
// #define UTEST_TEST_ASSERT_EQUAL_HEX64_ARRAY(expected, actual, num_elements, line)       utest_assert_equal_int_array((UTEST_INTERNAL_PTR)(expected), (UTEST_INTERNAL_PTR)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX64)
// #define UTEST_TEST_ASSERT_HEX64_WITHIN(delta, expected, actual, line)                   utest_assert_numbers_within((delta), (u_sint_t)(expected), (u_sint_t)(actual), (UTEST_LINE_TYPE)(line), UNITY_DISPLAY_STYLE_HEX64)

#define UTEST_TEST_ASSERT_FLOAT_WITHIN(delta, expected, actual, line, file)                   utest_assert_floats_within((float)(delta), (float)(expected), (float)(actual), (UTEST_LINE_TYPE)(line), file)
#define UTEST_TEST_ASSERT_EQUAL_FLOAT(tolerance, expected, actual, line, file)                UTEST_TEST_ASSERT_FLOAT_WITHIN((float)(tolerance), (float)(expected), (float)(actual), (UTEST_LINE_TYPE)(line), file)
#define UTEST_TEST_ASSERT_EQUAL_FLOAT_ARRAY(tolerance, expected, actual, num_elements, line, file)       utest_assert_equal_float_array((float)(tolerance), (float*)(expected), (float*)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)(line), file)
// #define UTEST_TEST_ASSERT_FLOAT_IS_INF(actual, line, file)                                    utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_INF)
// #define UTEST_TEST_ASSERT_FLOAT_IS_NEG_INF(actual, line, file)                                utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NEG_INF)
// #define UTEST_TEST_ASSERT_FLOAT_IS_NAN(actual, line, file)                                    utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NAN)
// #define UTEST_TEST_ASSERT_FLOAT_IS_DETERMINATE(actual, line, file)                            utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_DET)
// #define UTEST_TEST_ASSERT_FLOAT_IS_NOT_INF(actual, line, file)                                utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_INF)
// #define UTEST_TEST_ASSERT_FLOAT_IS_NOT_NEG_INF(actual, line, file)                            utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_NEG_INF)
// #define UTEST_TEST_ASSERT_FLOAT_IS_NOT_NAN(actual, line, file)                                utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_NAN)
// #define UTEST_TEST_ASSERT_FLOAT_IS_NOT_DETERMINATE(actual, line, file)                        utest_assert_float_condition((float)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_DET)

#define UTEST_TEST_ASSERT_DOUBLE_WITHIN(delta, expected, actual, line, file)                  utest_assert_doubles_within((double)(delta), (double)(expected), (double)(actual), (UTEST_LINE_TYPE)line, file)
#define UTEST_TEST_ASSERT_EQUAL_DOUBLE(tolerance, expected, actual, line, file)               UTEST_TEST_ASSERT_DOUBLE_WITHIN((double)(tolerance), (double)(expected), (double)actual, (UTEST_LINE_TYPE)(line), file)
#define UTEST_TEST_ASSERT_EQUAL_DOUBLE_ARRAY(tolerance, expected, actual, num_elements, line, file)      utest_assert_equal_double_array((double)(tolerance), (double*)(expected), (double*)(actual), (uu32_t)(num_elements), (UTEST_LINE_TYPE)line, file)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_INF(actual, line, file)                                   utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_INF)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_NEG_INF(actual, line, file)                               utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NEG_INF)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_NAN(actual, line, file)                                   utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NAN)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_DETERMINATE(actual, line, file)                           utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_DET)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_NOT_INF(actual, line, file)                               utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_INF)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_NOT_NEG_INF(actual, line, file)                           utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_NEG_INF)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_NOT_NAN(actual, line, file)                               utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_NAN)
// #define UTEST_TEST_ASSERT_DOUBLE_IS_NOT_DETERMINATE(actual, line, file)                       utest_assert_double_condition((double)(actual), (UTEST_LINE_TYPE)(line), UNITY_FLOAT_IS_NOT_DET)

/** API
 */

/* Boolean */
#define TEST_ASSERT(condition)                                                              UTEST_TEST_ASSERT((condition), __LINE__, __FILE__)
#define TEST_ASSERT_TRUE(condition)                                                         UTEST_TEST_ASSERT((condition), __LINE__, __FILE__)
#define TEST_ASSERT_UNLESS(condition)                                                       UTEST_TEST_ASSERT(!(condition), __LINE__, __FILE__)
#define TEST_ASSERT_FALSE(condition)                                                        UTEST_TEST_ASSERT(!(condition), __LINE__, __FILE__)
#define TEST_ASSERT_NULL(pointer)                                                           UTEST_TEST_ASSERT_NULL((pointer), __LINE__, __FILE__)
#define TEST_ASSERT_NOT_NULL(pointer)                                                       UTEST_TEST_ASSERT_NOT_NULL((pointer), __LINE__, __FILE__)
#define TEST_ASSERT_NOT_EQUAL(expected, actual)                                             UTEST_TEST_ASSERT(((expected) != (actual)), __LINE__, __FILE__)

/* Integers (of all sizes) */
#define TEST_ASSERT_EQUAL_INT(expected, actual)                                             UTEST_TEST_ASSERT_EQUAL_INT((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT8(expected, actual)                                            UTEST_TEST_ASSERT_EQUAL_INT8((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT16(expected, actual)                                           UTEST_TEST_ASSERT_EQUAL_INT16((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT32(expected, actual)                                           UTEST_TEST_ASSERT_EQUAL_INT32((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT64(expected, actual)                                           UTEST_TEST_ASSERT_EQUAL_INT64((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL(expected, actual)                                                 UTEST_TEST_ASSERT_EQUAL_INT((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT(expected, actual)                                            UTEST_TEST_ASSERT_EQUAL_UINT( (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT8(expected, actual)                                           UTEST_TEST_ASSERT_EQUAL_UINT8( (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT16(expected, actual)                                          UTEST_TEST_ASSERT_EQUAL_UINT16( (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT32(expected, actual)                                          UTEST_TEST_ASSERT_EQUAL_UINT32( (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT64(expected, actual)                                          UTEST_TEST_ASSERT_EQUAL_UINT64( (expected), (actual), __LINE__, __FILE__)
// #define TEST_ASSERT_EQUAL_HEX(expected, actual)                                         UTEST_TEST_ASSERT_EQUAL_HEX32((expected), (actual), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX8(expected, actual)                                        UTEST_TEST_ASSERT_EQUAL_HEX8( (expected), (actual), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX16(expected, actual)                                       UTEST_TEST_ASSERT_EQUAL_HEX16((expected), (actual), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX32(expected, actual)                                       UTEST_TEST_ASSERT_EQUAL_HEX32((expected), (actual), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX64(expected, actual)                                       UTEST_TEST_ASSERT_EQUAL_HEX64((expected), (actual), __LINE__,)
#define TEST_ASSERT_BITS(mask, expected, actual)                                            UTEST_TEST_ASSERT_BITS((mask), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_BITS_HIGH(mask, actual)                                                 UTEST_TEST_ASSERT_BITS((mask), (uu32_t)(-1), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_BITS_LOW(mask, actual)                                                  UTEST_TEST_ASSERT_BITS((mask), (uu32_t)(0), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_BIT_HIGH(bit, actual)                                                   UTEST_TEST_ASSERT_BITS(((uu32_t)1 << (bit)), (uu32_t)(-1), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_BIT_LOW(bit, actual)                                                    UTEST_TEST_ASSERT_BITS(((uu32_t)1 << (bit)), (uu32_t)(0), (actual), __LINE__, __FILE__)

/* Integer Ranges */
#define TEST_ASSERT_INT_WITHIN(delta, expected, actual)                                     UTEST_TEST_ASSERT_INT_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_INT8_WITHIN(delta, expected, actual)                                    UTEST_TEST_ASSERT_INT8_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_INT16_WITHIN(delta, expected, actual)                                   UTEST_TEST_ASSERT_INT16_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_INT32_WITHIN(delta, expected, actual)                                   UTEST_TEST_ASSERT_INT32_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_INT64_WITHIN(delta, expected, actual)                                   UTEST_TEST_ASSERT_INT64_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_UINT_WITHIN(delta, expected, actual)                                    UTEST_TEST_ASSERT_UINT_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_UINT8_WITHIN(delta, expected, actual)                                   UTEST_TEST_ASSERT_UINT8_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_UINT16_WITHIN(delta, expected, actual)                                  UTEST_TEST_ASSERT_UINT16_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_UINT32_WITHIN(delta, expected, actual)                                  UTEST_TEST_ASSERT_UINT32_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_UINT64_WITHIN(delta, expected, actual)                                  UTEST_TEST_ASSERT_UINT64_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
// #define TEST_ASSERT_HEX_WITHIN(delta, expected, actual)                                            UTEST_TEST_ASSERT_HEX32_WITHIN((delta), (expected), (actual), __LINE__)
// #define TEST_ASSERT_HEX8_WITHIN(delta, expected, actual)                                           UTEST_TEST_ASSERT_HEX8_WITHIN((delta), (expected), (actual), __LINE__)
// #define TEST_ASSERT_HEX16_WITHIN(delta, expected, actual)                                          UTEST_TEST_ASSERT_HEX16_WITHIN((delta), (expected), (actual), __LINE__)
// #define TEST_ASSERT_HEX32_WITHIN(delta, expected, actual)                                          UTEST_TEST_ASSERT_HEX32_WITHIN((delta), (expected), (actual), __LINE__)
// #define TEST_ASSERT_HEX64_WITHIN(delta, expected, actual)                                          UTEST_TEST_ASSERT_HEX64_WITHIN((delta), (expected), (actual), __LINE__)

/* Structs and Strings */
#define TEST_ASSERT_EQUAL_PTR(expected, actual)                                             UTEST_TEST_ASSERT_EQUAL_PTR((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_MEMORY(expected, actual, len)                                     UTEST_TEST_ASSERT_EQUAL_MEMORY((expected), (actual), (len), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_STRING(expected, actual)                                          UTEST_TEST_ASSERT_EQUAL_STRING((expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_STRING_TILL_LEN(expected, actual, len)                            UTEST_TEST_ASSERT_EQUAL_STRING_TILL_LEN((expected), (actual), (len), __LINE__, __FILE__)
// #define TEST_ASSERT_EQUAL_PTR_ARRAY(expected, actual, num_elements)                                UTEST_TEST_ASSERT_EQUAL_PTR_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
// #define TEST_ASSERT_EQUAL_STRING_ARRAY(expected, actual, num_elements)                             UTEST_TEST_ASSERT_EQUAL_STRING_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
// #define TEST_ASSERT_EQUAL_MEMORY_ARRAY(expected, actual, len, num_elements)                        UTEST_TEST_ASSERT_EQUAL_MEMORY_ARRAY((expected), (actual), (len), (num_elements), __LINE__, __FILE__)

/* Arrays */
#define TEST_ASSERT_EQUAL_INT_ARRAY(expected, actual, num_elements)                         UTEST_TEST_ASSERT_EQUAL_INT_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT8_ARRAY(expected, actual, num_elements)                        UTEST_TEST_ASSERT_EQUAL_INT8_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT16_ARRAY(expected, actual, num_elements)                       UTEST_TEST_ASSERT_EQUAL_INT16_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT32_ARRAY(expected, actual, num_elements)                       UTEST_TEST_ASSERT_EQUAL_INT32_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_INT64_ARRAY(expected, actual, num_elements)                       UTEST_TEST_ASSERT_EQUAL_INT64_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT_ARRAY(expected, actual, num_elements)                        UTEST_TEST_ASSERT_EQUAL_UINT_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, actual, num_elements)                       UTEST_TEST_ASSERT_EQUAL_UINT8_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT16_ARRAY(expected, actual, num_elements)                      UTEST_TEST_ASSERT_EQUAL_UINT16_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT32_ARRAY(expected, actual, num_elements)                      UTEST_TEST_ASSERT_EQUAL_UINT32_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_UINT64_ARRAY(expected, actual, num_elements)                      UTEST_TEST_ASSERT_EQUAL_UINT64_ARRAY((expected), (actual), (num_elements), __LINE__, __FILE__)
// #define TEST_ASSERT_EQUAL_HEX_ARRAY(expected, actual, num_elements)                                UTEST_TEST_ASSERT_EQUAL_HEX32_ARRAY((expected), (actual), (num_elements), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX8_ARRAY(expected, actual, num_elements)                               UTEST_TEST_ASSERT_EQUAL_HEX8_ARRAY((expected), (actual), (num_elements), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX16_ARRAY(expected, actual, num_elements)                              UTEST_TEST_ASSERT_EQUAL_HEX16_ARRAY((expected), (actual), (num_elements), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX32_ARRAY(expected, actual, num_elements)                              UTEST_TEST_ASSERT_EQUAL_HEX32_ARRAY((expected), (actual), (num_elements), __LINE__)
// #define TEST_ASSERT_EQUAL_HEX64_ARRAY(expected, actual, num_elements)                              UTEST_TEST_ASSERT_EQUAL_HEX64_ARRAY((expected), (actual), (num_elements), __LINE__)

/* Floating Point */
#define TEST_ASSERT_FLOAT_WITHIN(delta, expected, actual)                                   UTEST_TEST_ASSERT_FLOAT_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_FLOAT(tolerance, expected, actual)                                UTEST_TEST_ASSERT_EQUAL_FLOAT((tolerance), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_FLOAT_ARRAY(tolerance, expected, actual, num_elements)            UTEST_TEST_ASSERT_EQUAL_FLOAT_ARRAY((tolerance), (expected), (actual), (num_elements), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_INF(actual)                                                           UTEST_TEST_ASSERT_FLOAT_IS_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_NEG_INF(actual)                                                       UTEST_TEST_ASSERT_FLOAT_IS_NEG_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_NAN(actual)                                                           UTEST_TEST_ASSERT_FLOAT_IS_NAN((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_DETERMINATE(actual)                                                   UTEST_TEST_ASSERT_FLOAT_IS_DETERMINATE((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_NOT_INF(actual)                                                       UTEST_TEST_ASSERT_FLOAT_IS_NOT_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_NOT_NEG_INF(actual)                                                   UTEST_TEST_ASSERT_FLOAT_IS_NOT_NEG_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_NOT_NAN(actual)                                                       UTEST_TEST_ASSERT_FLOAT_IS_NOT_NAN((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_FLOAT_IS_NOT_DETERMINATE(actual)                                               UTEST_TEST_ASSERT_FLOAT_IS_NOT_DETERMINATE((actual), __LINE__, __FILE__)

/* Double */
#define TEST_ASSERT_DOUBLE_WITHIN(delta, expected, actual)                                  UTEST_TEST_ASSERT_DOUBLE_WITHIN((delta), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_DOUBLE(tolerance, expected, actual)                               UTEST_TEST_ASSERT_EQUAL_DOUBLE((tolerance), (expected), (actual), __LINE__, __FILE__)
#define TEST_ASSERT_EQUAL_DOUBLE_ARRAY(tolerance, expected, actual, num_elements)           UTEST_TEST_ASSERT_EQUAL_DOUBLE_ARRAY((tolerance), (expected), (actual), (num_elements), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_INF(actual)                                                          UTEST_TEST_ASSERT_DOUBLE_IS_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_NEG_INF(actual)                                                      UTEST_TEST_ASSERT_DOUBLE_IS_NEG_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_NAN(actual)                                                          UTEST_TEST_ASSERT_DOUBLE_IS_NAN((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_DETERMINATE(actual)                                                  UTEST_TEST_ASSERT_DOUBLE_IS_DETERMINATE((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_NOT_INF(actual)                                                      UTEST_TEST_ASSERT_DOUBLE_IS_NOT_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_NOT_NEG_INF(actual)                                                  UTEST_TEST_ASSERT_DOUBLE_IS_NOT_NEG_INF((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_NOT_NAN(actual)                                                      UTEST_TEST_ASSERT_DOUBLE_IS_NOT_NAN((actual), __LINE__, __FILE__)
// #define TEST_ASSERT_DOUBLE_IS_NOT_DETERMINATE(actual)                                              UTEST_TEST_ASSERT_DOUBLE_IS_NOT_DETERMINATE((actual), __LINE__, __FILE__)

#endif /* APRIL_UTEST */
