/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

#include "utest.h"

/** Magic Strings
 */

static const char utest_str_magic[]     = "UTEST";
static const char utest_str_ok[]        = "OK";
static const char utest_str_pass[]      = "PASS";
static const char utest_str_fail[]      = "FAIL";
static const char utest_str_expected[]  = " Expected ";
static const char utest_str_actual[]    = " Actual ";
static const char utest_str_break[]     = "-----------------------";
static const char utest_str_delta[]     = " Values Not Within Delta ";
static const char utest_str_element[]   = " Element ";
static const char utest_str_tolerance[] = " Values Not Within Tolerance ";
static const char utest_str_length[]    = " length ";
static const char utest_str_mem_miss[]  = " Memory Mismatch.";
static const char utest_str_byte[]      = " Byte ";
static const char utest_str_condition[] = " Condition ";
static const char *filename             = NULL;

void utest_print_start_test()
{
    printf("%s ", utest_str_magic);
}

void utest_print_test_location(const char *file, int line)
{
    printf("%s %d ", file, line);
}

void utest_print_end_test()
{
    printf("\n");
}

void utest_print_number_unsigned(const u_uint_t number)
{
    u_uint_t divisor = 1;

    /* figure out initial divisor */
    while (number / divisor > 9)
    {
        divisor *= 10;
    }

    do
    {
        printf("%c", ((char)('0' + (number / divisor % 10))));
        divisor /= 10;
    }
    while (divisor > 0);
    printf(" ");
}

void utest_print_number(const u_sint_t number_to_print)
{
    u_uint_t number = (u_uint_t)number_to_print;

    if (number < 0)
    {
        /* A negative number, including MIN negative */
        printf("-");
        number = (u_uint_t)(-number_to_print);
    }
    utest_print_number_unsigned(number);
}

void utest_print_number_with_style(const u_sint_t number,
                                   const UTEST_DISPLAY_STYLE_T style)
{
    if ((style & UTEST_DISPLAY_RANGE_INT) == UTEST_DISPLAY_RANGE_INT)
    {
        utest_print_number(number);
    }
    else if ((style & UTEST_DISPLAY_RANGE_UINT) == UTEST_DISPLAY_RANGE_UINT)
    {
        utest_print_number_unsigned((u_uint_t)number  &
                                    UTEST_SIZE_MASK[((u_uint_t)style &
                                                     (u_uint_t)0x0F) - 1]  );
    }
}

void utest_assert_condition(const bool condition,
                            const UTEST_LINE_TYPE line_number,
                            const char *file)
{
    utest_print_start_test();
    if (condition) {
        printf("%s ", utest_str_pass);
    } else {
        printf("%s ", utest_str_fail);
        printf("%s ", utest_str_condition);
    }
    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_equal_bits(const u_sint_t mask,
                             const u_sint_t expected,
                             const u_sint_t actual,
                             const UTEST_LINE_TYPE line_number,
                             const char *file)
{
    utest_print_start_test();
    if ((mask & expected) != (mask & actual)) {
        printf("%s ", utest_str_fail);
        printf("%s %d ", utest_str_expected, mask & expected);
        printf("%s %d ", utest_str_actual, mask & actual);
    } else {
        printf("%s ", utest_str_pass);
    }
    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_equal_number(const u_sint_t expected,
                               const u_sint_t actual,
                               const UTEST_LINE_TYPE line_number,
                               const char *file,
                               const UTEST_DISPLAY_STYLE_T style)
{
    utest_print_start_test();
    if (expected != actual) {
        printf("%s ", utest_str_fail);
        printf("%s ", utest_str_expected);
        utest_print_number_with_style(expected, style);
        printf("%s ", utest_str_actual);
        utest_print_number_with_style(actual, style);
    } else {
        printf("%s ", utest_str_pass);
    }
    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_number_within(const u_uint_t delta,
                                const u_sint_t expected,
                                const u_sint_t actual,
                                const UTEST_LINE_TYPE line_number,
                                const char *file,
                                const UTEST_DISPLAY_STYLE_T style)
{
    utest_print_start_test();

    bool test_failed = false;
    if ((style & UTEST_DISPLAY_RANGE_INT) == UTEST_DISPLAY_RANGE_INT) {
        if (actual > expected) {
            if ((actual - expected) > delta)
                test_failed = true;
        } else {
            if ((expected - actual) > delta)
                test_failed = true;
        }
    } else {
        if ((u_uint_t)actual > (u_uint_t)expected){
            if ((u_uint_t)(actual - expected) > delta)
                test_failed = true;
        } else {
            if ((u_uint_t)(expected - actual) > delta)
                test_failed = true;
        }
    }

    if (test_failed) {
        printf("%s ", utest_str_fail);
        printf("%s ", utest_str_delta);
        utest_print_number_with_style(delta, style);
        printf("%s ", utest_str_expected);
        utest_print_number_with_style(expected, style);
        printf("%s ", utest_str_actual);
        utest_print_number_with_style(actual, style);
    } else {
        printf("%s ", utest_str_pass);
    }

    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_equal_int_array(UTEST_INTERNAL_PTR expected,
                                  UTEST_INTERNAL_PTR actual,
                                  const uu32_t num_elements,
                                  const UTEST_LINE_TYPE line_number,
                                  const char *file,
                                  const UTEST_DISPLAY_STYLE_T style)
{
    uu32_t elements = num_elements;
    UTEST_INTERNAL_PTR ptr_exp = expected;
    UTEST_INTERNAL_PTR ptr_act = actual;

    utest_print_start_test();

    if (elements == 0)
    {
        printf("%s ", utest_str_pass);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    }

    if (expected == NULL || actual == NULL) {
        printf("%s ", utest_str_fail);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    }

    switch(style & (UTEST_DISPLAY_STYLE_T)(~UTEST_DISPLAY_RANGE_AUTO))
    {
        case UTEST_DISPLAY_STYLE_INT8:
        case UTEST_DISPLAY_STYLE_UINT8:
            ptr_exp = ((const us8_t*)ptr_exp);
            ptr_act = ((const us8_t*)ptr_act);
            while (elements--)
            {
                if (*(const us8_t*)ptr_exp != *(const us8_t*)ptr_act)
                {
                    printf("%s ", utest_str_fail);
                    printf("%s ", utest_str_element);
                    utest_print_number_unsigned(num_elements - elements - 1);
                    printf("%s ", utest_str_expected);
                    utest_print_number_with_style(*(const us8_t*)ptr_exp, style);
                    printf("%s ", utest_str_actual);
                    utest_print_number_with_style(*(const us8_t*)ptr_act, style);
                    utest_print_test_location(file, line_number);
                    utest_print_end_test();
                    return;
                }
                ptr_exp = ((const us8_t*)ptr_exp + 1);
                ptr_act = ((const us8_t*)ptr_act + 1);
            }
            break;
        case UTEST_DISPLAY_STYLE_INT16:
        case UTEST_DISPLAY_STYLE_UINT16:
            ptr_exp = ((const us16_t*)ptr_exp);
            ptr_act = ((const us16_t*)ptr_act);
            while (elements--)
            {
                if (*(const us16_t*)ptr_exp != *(const us16_t*)ptr_act)
                {
                    printf("%s ", utest_str_fail);
                    printf("%s ", utest_str_element);
                    utest_print_number_unsigned(num_elements - elements - 1);
                    printf("%s ", utest_str_expected);
                    utest_print_number_with_style(*(const us16_t*)ptr_exp, style);
                    printf("%s ", utest_str_actual);
                    utest_print_number_with_style(*(const us16_t*)ptr_act, style);
                    utest_print_test_location(file, line_number);
                    utest_print_end_test();
                    return;
                }
                ptr_exp = ((const us16_t*)ptr_exp + 1);
                ptr_act = ((const us16_t*)ptr_act + 1);
            }
            break;

        #ifdef UTEST_SUPPORT_64
        case UTEST_DISPLAY_STYLE_INT64:
        case UTEST_DISPLAY_STYLE_UINT64:
            ptr_exp = ((const us64_t*)ptr_exp);
            ptr_act = ((const us64_t*)ptr_act);
            while (elements--)
            {
                if (*(const us64_t*)ptr_exp != *(const us64_t*)ptr_act)
                {
                    printf("%s ", utest_str_fail);
                    printf("%s ", utest_str_element);
                    utest_print_number_unsigned(num_elements - elements - 1);
                    printf("%s ", utest_str_expected);
                    utest_print_number_with_style(*(const us64_t*)ptr_exp, style);
                    printf("%s ", utest_str_actual);
                    utest_print_number_with_style(*(const us64_t*)ptr_act, style);
                    utest_print_test_location(file, line_number);
                    utest_print_end_test();
                    return;
                }
                ptr_exp = ((const us64_t*)ptr_exp + 1);
                ptr_act = ((const us64_t*)ptr_act + 1);
            }
            break;
        #endif
        default:
            ptr_exp = ((const us32_t*)ptr_exp);
            ptr_act = ((const us32_t*)ptr_act);
            while (elements--)
            {
                if (*(const us32_t*)ptr_exp != *(const us32_t*)ptr_act)
                {
                    printf("%s ", utest_str_fail);
                    printf("%s ", utest_str_element);
                    utest_print_number_unsigned(num_elements - elements - 1);
                    printf("%s ", utest_str_expected);
                    utest_print_number_with_style(*(const us32_t*)ptr_exp, style);
                    printf("%s ", utest_str_actual);
                    utest_print_number_with_style(*(const us32_t*)ptr_act, style);
                    utest_print_test_location(file, line_number);
                    utest_print_end_test();
                    return;
                }
                ptr_exp = ((const us32_t*)ptr_exp + 1);
                ptr_act = ((const us32_t*)ptr_act + 1);
            }
            break;
    }
    printf("%s ", utest_str_pass);
    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_floats_within(const u_float_t delta,
                                const u_float_t expected,
                                const u_float_t actual,
                                const UTEST_LINE_TYPE line_number,
                                const char *file)
{
    utest_print_start_test();

    u_float_t diff = actual - expected;
    u_float_t pos_delta = delta;

    if (diff < 0.0f) {
        diff = 0.0f - diff;
    }
    if (pos_delta < 0.0f) {
        pos_delta = 0.0f - pos_delta;
    }

    if (isnan(diff) || isinf(diff) || (pos_delta < diff)) {
        printf("%s ", utest_str_fail);
        printf("%s ", utest_str_delta);
        printf("%f ", delta);
        printf("%s ", utest_str_expected);
        printf("%f ", expected);
        printf("%s ", utest_str_actual);
        printf("%f ", actual);

    } else {
        printf("%s ", utest_str_pass);
    }

    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_equal_float_array(const u_float_t tolerance,
                                    const u_float_t* expected,
                                    const u_float_t* actual,
                                    const uu32_t num_elements,
                                    const UTEST_LINE_TYPE line_number,
                                    const char* file)
{
    utest_print_start_test();

    uu32_t elements = num_elements;
    const u_float_t* ptr_expected = expected;
    const u_float_t* ptr_actual = actual;
    u_float_t diff, tol = tolerance;

    if (elements == 0 || (expected == NULL && actual == NULL))
    {
        printf("%s ", utest_str_pass);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    } else if (expected == NULL || actual == NULL) {
        printf("%s ", utest_str_fail);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    }

    while (elements--)
    {
        diff = *ptr_expected - *ptr_actual;
        if (diff < 0.0f)
            diff = 0.0f - diff;
        // tol = UTEST_FLOAT_PRECISION * *ptr_expected;
        if (tol < 0.0f)
            tol = 0.0f - tol;

        if (isnan(diff) || isinf(diff) || (diff > tol))
        {
            printf("%s ", utest_str_fail);
            printf("%s ", utest_str_element);
            utest_print_number_unsigned(num_elements - elements - 1);
            printf("%s ", utest_str_tolerance);
            printf("%f ", tol);
            printf("%s ", utest_str_expected);
            printf("%f ", *ptr_expected);
            printf("%s ", utest_str_actual);
            printf("%f ", *ptr_actual);
            utest_print_test_location(file, line_number);
            utest_print_end_test();
            return;
        }
        ptr_expected++;
        ptr_actual++;
    }

    printf("%s ", utest_str_pass);
    utest_print_test_location(file, line_number);
    utest_print_end_test();
}


void utest_assert_doubles_within(const u_double_t delta,
                                 const u_double_t expected,
                                 const u_double_t actual,
                                 const UTEST_LINE_TYPE line_number,
                                 const char *file)
{
    utest_print_start_test();

    u_double_t diff = actual - expected;
    u_double_t pos_delta = delta;

    if (diff < 0.0)
    {
        diff = 0.0 - diff;
    }
    if (pos_delta < 0.0)
    {
        pos_delta = 0.0 - pos_delta;
    }

    if (isnan(diff) || isinf(diff) || (pos_delta < diff))
    {
        printf("%s ", utest_str_fail);
        printf("%s ", utest_str_delta);
        printf("%lf ", delta);
        printf("%s ", utest_str_expected);
        printf("%lf ", expected);
        printf("%s ", utest_str_actual);
        printf("%lf ", actual);
    } else {
        printf("%s ", utest_str_pass);
    }

    utest_print_test_location(file, line_number);
    utest_print_end_test();
}


void utest_assert_equal_double_array(const u_double_t tolerance,
                                     const u_double_t* expected,
                                     const u_double_t* actual,
                                     const uu32_t num_elements,
                                     const UTEST_LINE_TYPE line_number,
                                     const char* file)
{
    utest_print_start_test();

    uu32_t elements = num_elements;
    const u_double_t* ptr_expected = expected;
    const u_double_t* ptr_actual = actual;
    u_double_t diff, tol = tolerance;

    if (elements == 0 || (expected == NULL && actual == NULL))
    {
        printf("%s ", utest_str_pass);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    } else if (expected == NULL || actual == NULL) {
        printf("%s ", utest_str_fail);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    }

    while (elements--)
    {
        diff = *ptr_expected - *ptr_actual;
        if (diff < 0.0)
          diff = 0.0 - diff;

        if (tol < 0.0)
            tol = 0.0 - tol;

        if (isnan(diff) || isinf(diff) || (diff > tol))
        {
            printf("%s ", utest_str_fail);
            printf("%s ", utest_str_element);
            utest_print_number_unsigned(num_elements - elements - 1);
            printf("%s ", utest_str_tolerance);
            printf("%lf ", tol);
            printf("%s ", utest_str_expected);
            printf("%lf ", *ptr_expected);
            printf("%s ", utest_str_actual);
            printf("%lf ", *ptr_actual);
            utest_print_test_location(file, line_number);
            utest_print_end_test();
            return;
        }
        ptr_expected++;
        ptr_actual++;
    }
    printf("%s ", utest_str_pass);
    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_equal_string(const char* expected,
                               const char* actual,
                               const UTEST_LINE_TYPE line_number,
                               const char* file)
{
    utest_print_start_test();

    uu32_t i;
    bool test_failed = false;

    /* if both pointers not null compare the strings */
    if (expected && actual)
    {
        for (i = 0; expected[i] || actual[i]; i++)
        {
            if (expected[i] != actual[i])
            {
                test_failed = true;
                break;
            }
        }
    }
    else
    {
        if (expected != actual)
        {
            test_failed = true;
        }
    }

    if (test_failed) {
        printf("%s ", utest_str_fail);
        printf("%s ", utest_str_expected);
        printf("%s ", expected);
        printf("%s ", utest_str_actual);
        printf("%s ", actual);
    } else {
        printf("%s ", utest_str_pass);
    }

    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_equal_string_till_len(const char* expected,
                                          const char* actual,
                                          const uu32_t length,
                                          const UTEST_LINE_TYPE line_number,
                                          const char* file)
{
    utest_print_start_test();

    uu32_t i;
    bool test_failed = false;

    /* if both pointers not null compare the strings */
    if (expected && actual)
    {
        for (i = 0; (expected[i] || actual[i]) && i < length; i++)
        {
            if (expected[i] != actual[i])
            {
                test_failed = true;
                break;
            }
        }
    }
    else
    {
        if (expected != actual)
        {
            test_failed = true;
        }
    }

    if (test_failed) {
        printf("%s ", utest_str_fail);
        printf("%s ", utest_str_length);
        utest_print_number_unsigned(length);
        printf("%s ", utest_str_expected);
        printf("%s ", expected);
        printf("%s ", utest_str_actual);
        printf("%s ", actual);
    } else {
        printf("%s ", utest_str_pass);
    }

    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_assert_equal_memory(UTEST_INTERNAL_PTR expected,
                               UTEST_INTERNAL_PTR actual,
                               const uu32_t length,
                               const uu32_t num_elements,
                               const UTEST_LINE_TYPE line_number,
                               const char* file)
{
    utest_print_start_test();

    const unsigned char* ptr_exp = (const unsigned char*)expected;
    const unsigned char* ptr_act = (const unsigned char*)actual;
    uu32_t elements = num_elements;
    uu32_t bytes;

    if (elements == 0 || length == 0 || (expected == NULL && actual == NULL))
    {
        printf("%s ", utest_str_pass);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    } else if (expected == NULL || actual == NULL) {
        printf("%s ", utest_str_fail);
        utest_print_test_location(file, line_number);
        utest_print_end_test();
        return;
    }

    while (elements--)
    {
        bytes = length;
        while (bytes--)
        {
            if (*ptr_exp != *ptr_act)
            {
                printf("%s ", utest_str_fail);
                printf("%s ", utest_str_mem_miss);
                printf("%s ", utest_str_element);
                utest_print_number_unsigned(num_elements - elements - 1);
                printf("%s ", utest_str_expected);
                utest_print_number_with_style(*(const u_ptr_t*)ptr_exp, UTEST_DISPLAY_STYLE_POINTER);
                printf("%s ", utest_str_actual);
                utest_print_number_with_style(*(const u_ptr_t*)ptr_act, UTEST_DISPLAY_STYLE_POINTER);
                utest_print_test_location(file, line_number);
                utest_print_end_test();
                return;
            }
            ptr_exp = ptr_exp + 1;
            ptr_act = ptr_act + 1;
        }
    }
    printf("%s ", utest_str_pass);
    utest_print_test_location(file, line_number);
    utest_print_end_test();
}

void utest_test_suite_begin(const char * file)
{
    filename = file;
}

void utest_test_suite_end(const char * file)
{
    filename = NULL;
}
