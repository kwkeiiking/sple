.PHONY: ats ats_clean
export CFLAGS_ATS =
export LDFLAGS_ATS = $(LDFLAGS_APRIL_LCMTYPES)
export DEPS_ATS = $(LIB_PATH)/libats.a

ats: common april_lcmtypes

ats:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/ats -f Build.mk

ats_clean:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/ats -f Build.mk clean

all: ats

clean: ats_clean
