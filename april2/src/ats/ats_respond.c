/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

#include "ats.h"

int main()
{
    setlinebuf(stdout);
    printf("Starting Pong Side...\n");

    // create UDP socket
    int send_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (send_socket < 0) {
        perror("socket");
        return -1;
    }

    int v = 1;
    if (setsockopt(send_socket, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v))) {
        perror("setsockopt");
        return -1;
    }

    int recv_sock = udp_socket_listen(PING_PORT);
    if (recv_sock < 0) {
        printf("UDP Socket Listen\n");
        return -1;
    }

    while (1) {
        uint8_t rx[ATS_PACKET_SIZE];
        uint32_t rxpos = 0;

        struct sockaddr_in addr;
        socklen_t src_addr_len = sizeof(addr);

        ssize_t rxlen = recvfrom(recv_sock, rx, ATS_PACKET_SIZE, 0,
                                 (struct sockaddr*) &addr, &src_addr_len);
        int64_t this_time = utime_now();

        if(rxlen != ATS_PACKET_SIZE) {
            printf("Bad length %zd\n", rxlen);
            continue;
        }

        if (rx[0] != ((ATS_PING_MAGIC >> 24) & 0xff) ||
            rx[1] != ((ATS_PING_MAGIC >> 16) & 0xff) ||
            rx[2] != ((ATS_PING_MAGIC >>  8) & 0xff) ||
            rx[3] != ((ATS_PING_MAGIC >>  0) & 0xff) ) {
            printf("Bad magic %d\n", rx[3]);
            continue;
        }

        rx[0] = (ATS_PONG_MAGIC >> 24) & 0xff;
        rx[1] = (ATS_PONG_MAGIC >> 16) & 0xff;
        rx[2] = (ATS_PONG_MAGIC >>  8) & 0xff;
        rx[3] = (ATS_PONG_MAGIC >>  0) & 0xff;

        int32_t port = 0;
        for (int i = 0; i < 4; i++) {
            port = (port << 8) + rx[i+4];
        }
        addr.sin_port = htons(port);

        int64_t old_time = 0;
        for (int i = 0; i < 8; i++) {
            old_time = (old_time << 8) + rx[i+8];
        }

        int64_t time_diff = this_time - old_time;
        rx[4] = (time_diff >> 56) & 0xff;
        rx[5] = (time_diff >> 48) & 0xff;
        rx[6] = (time_diff >> 40) & 0xff;
        rx[7] = (time_diff >> 32) & 0xff;
        rx[8] = (time_diff >> 24) & 0xff;
        rx[9] = (time_diff >> 16) & 0xff;
        rx[10] = (time_diff >>  8) & 0xff;
        rx[11] = (time_diff >>  0) & 0xff;

        int64_t host_time = utime_now();
        rx[12] = (host_time >> 56) & 0xff;
        rx[13] = (host_time >> 48) & 0xff;
        rx[14] = (host_time >> 40) & 0xff;
        rx[15] = (host_time >> 32) & 0xff;
        rx[16] = (host_time >> 24) & 0xff;
        rx[17] = (host_time >> 16) & 0xff;
        rx[18] = (host_time >>  8) & 0xff;
        rx[19] = (host_time >>  0) & 0xff;

        ssize_t txlen = sendto(send_socket, rx, ATS_PACKET_SIZE, 0, (struct sockaddr*) &addr, sizeof(addr));
        if(txlen != ATS_PACKET_SIZE) {
            printf("Bad sendlen %zd to port %d\n", txlen, port);
            continue;
        }
    }
}
