# Fast and Robust 3D Feature Extraction from Sparse Point Clouds \[[pdf](http://jacoposerafin.com/wp-content/uploads/serafin16iros.pdf)\]

Jacopo Serafin and Giorgio Grisetti

**Abstract**

Point cloud registration is a fundamental building block of many robotic 
applications. In this paper we describe a system to solve the registration 
problem, that builds on top of our previous work [1], and that represents an 
extension to the well known Iterative Closest Point (ICP) algorithm. Our approach 
combines recent achievements on optimization by using an extended point 
representation [2] that captures the surface characteristics around the points. 
Thanks to an effective strategy to search for correspondences, our method can 
operate on-line and cope with measurements gathered with an heterogeneous set of
range and depth sensors. By using an efficient map-merging procedure our 
approach can quickly update the tracked scene and handle dynamic aspects. We 
also introduce an approximated variant of our method that runs at twice the 
speed of our full implementation. Experiments performed on a large publicly 
available benchmarking dataset show that our approach performs better with 
respect to other state-of-the art methods. In most of the tests considered, our
algorithm has been able to obtain a translational and rotational relative error 
of respectively ∼1 cm and ∼1 degree.

# What

C code for the ectraction of 3D planes and lines from sparse data such as that of
a Velodyne sensor

# Requirements

Lightweight Communications and Marshalling (LCM) \[[link](https://lcm-proj.github.io/)\]

# Compiling

./make

