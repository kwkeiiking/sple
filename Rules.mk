#april2
# include $(ROOT_PATH)/april2/Rules.mk

#define libs
LIBLCM := $(APRIL_LIB_PATH)/libaprillcmtypes.a
LIBCOMMON := $(APRIL_LIB_PATH)/libcommon.a
LIBEK := $(APRIL_LIB_PATH)/libek.a
LIBVX := $(APRIL_LIB_PATH)/libvx.a
LIBHTTPD := $(APRIL_LIB_PATH)/libaprilhttpd.a
LIBGRAPH := $(APRIL_LIB_PATH)/libgraph.a

LIBLCMTYPES := $(LIB_PATH)/libsclcmtypes.a $(LIBLCM)
LIBUTILS := $(LIB_PATH)/libutils.a
LIBVELODYNE32FEATURES := $(LIB_PATH)/libvelodyne32features.a
LIBVELODYNE32FEATURESSLAM := $(LIB_PATH)/libvelodyne32featuresslam.a

LDFLAGS_APRIL :=  $(LIB_PATH)/libvx.a $(LIB_PATH)/libgraph.a $(LIB_PATH)/libhttpd.a $(LIB_PATH)/libek.a $(LIB_PATH)/libcommon.a $(LIB_PATH)/libaprillcmtypes.a $(LIB_PATH)/libsim.a -lm -pthread

#source
D	:= $(D)/src
include		$(D)/Rules.mk
D       := $(realpath $(dir $(D)))
